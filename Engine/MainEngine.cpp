/*NAME:		MainEngine.h
* AUTHOR:	Yoran Staal
*
* DESCRIPTION: The core of the control part of the software. The GUI sends all updates to
* this class, and they will be handles here. The updates will be received intantaneously.
* There is also a seperate thread that takes care of frame updates (30 FPS). Dependend on
* the mode that the engine is in it will write the static RGB values or let the fxEngine
* class take care of it.
*/

#include "MainEngine.h"
#include "Config/Configuration.h"
#include "Scripting/LuaFunctionLoader.h"

#include <algorithm>

MainEngine::MainEngine(MutexedData &rgbData,
                       const std::optional<std::string> &luaScriptPath)
    : mRGBData(rgbData)
{
    mConfig = ConfigurationManager::loadConfig();
	fxFiles = new EffectFileHandler();

	// Reserve enough space for the rgb data
	leds.reserve(mConfig.numberOfLeds * 3);
	for (uint32_t i = 0; i < mConfig.numberOfLeds * 3; i++) {
		leds.push_back(0x00);
	}

	fxFiles->getEffectsInfo(&allEffects);

	// Initialize static color
	staticColor.r = 255;
	staticColor.g = 255;
	staticColor.b = 255;
	staticColor.d = 255;

    mLuaEngine.initLua();
    // Load the functions used to 
    LFL::loadFunctions(mLuaEngine, *this);

	// Start a seperate thread that handles periodic updates
	std::thread timingTrigger(&MainEngine::updateInterrupt, this);
	timingTrigger.detach();

    if (luaScriptPath)
    {
        auto func = std::bind(&LuaEngine::runFile, &mLuaEngine, luaScriptPath.value());
        mLuaScriptThread = std::thread(func);
    }
}


MainEngine::~MainEngine() 
{
    mLuaEngine.destroyLua();
    mIsRunning = false;

    while (!mCanClose)
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    delete fxFiles;
}


// Timer that calls the update function. Clock lines for debugging purposes
void MainEngine::updateInterrupt() 
{
	// std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
	// std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

	while (mIsRunning) {
		// end = std::chrono::steady_clock::now();
		// wxLogDebug("<TimingEngine> frame time %d [ms]", std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count());
		// begin = std::chrono::steady_clock::now();
		std::this_thread::sleep_for(std::chrono::microseconds(INTERVAL));
		update();
	}

    mCanClose = true;
}


std::vector<EffectInfo>* MainEngine::getEffectList() {
	return &allEffects;
}


void MainEngine::update() 
{
	setColorAll(colorZero);			// Set all leds to 0
	effectUpdate();					// Handle all effects that are running
	mRGBData.setData(leds);			// Set the data for the output stage
}


// Function that runs all effects and determines final RGB values.
void MainEngine::effectUpdate() 
{
    for (auto runner : runningEffects)
        runner->effectUpdate();
}


void MainEngine::effectSelection(uint16_t id) {
	curFxSelId = id+1;
	wxLogDebug("<MainEngine> New effect selected: %d", curFxSelId);
}


FxEditor *MainEngine::newEffectEditor()
{
	tempFx = fxFiles->getEffect(curFxSelId);
	FxEditor *fxEditor = new FxEditor(&leds, &tempFx);

    this->addRunner(fxEditor);
	return fxEditor;
}

void MainEngine::addRunner(FxRunner* runner)
{
    runningEffects.push_back(runner);
}

void MainEngine::removeRunner(FxRunner* runner)
{
	auto it = std::find(runningEffects.begin(), runningEffects.end(), runner);
	runningEffects.erase(it);
}


void MainEngine::toolBarInput(uint16_t id) 
{
	switch (id)
	{
	case wxID_TOOL_EDIT:
		break;
	}
}


void MainEngine::inputBtnChange(uint32_t buttonID) 
{
	switch (buttonID) {
	case wxID_MODE_STATCOL:
		mode = staticMode;
		break;
	case wxID_MODE_RAINBOW:
		mode = effectMode1;
		break;
	case wxID_MODE_EFFECT:
		mode = effectMode2;
		break;
	}
}


void MainEngine::inputSldChange(uint32_t sliderID, uint32_t value) 
{
	switch (sliderID) {
	case wxID_SLD_STATCOL_DIM:
		dim = value;
		break;
	case wxID_SLD_STATCOL_RED:
		staticColor.r = value;
		break;
	case wxID_SLD_STATCOL_GREEN:
		staticColor.g = value;
		break;
	case wxID_SLD_STATCOL_BLUE:
		staticColor.b = value;
		break;
	}
}


//Function that sets the color of a single LED
void MainEngine::setColorSingle(uint16_t ledNr, RgbColor col) {
	uint16_t ledIt = 3 * ledNr;
	leds[ledIt] = col.r * dim / 255;
	leds[ledIt+1] = col.g * dim / 255;
	leds[ledIt+2] = col.b * dim / 255;
}


void MainEngine::setColorAll(RgbColor col) {
	for (unsigned i = 0; i < 3 * mConfig.numberOfLeds; i += 3) {
		leds[i] = col.r * col.d / 255;
		leds[i + 1] = col.g * col.d / 255;
		leds[i + 2] = col.b * col.d / 255;
	}	
}


void MainEngine::onSettingsChange()
{
    mConfig = ConfigurationManager::loadConfig();

    // Resize all objects that contain buffers 
    mRGBData.resize(mConfig.numberOfLeds * 3);
	leds.resize(mConfig.numberOfLeds * 3);
}
