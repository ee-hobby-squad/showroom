
#include "FxEditor.h"
/**/
FxEditor::FxEditor(std::vector<uint8_t>* led, Effect* FX) : FxRunner(led, FX)
{
	fileHandler = new EffectFileHandler();
}


FxEditor::~FxEditor()
{

}


void FxEditor::setFxParameter(FxParameter fxPrm, float value)
{
	wxLogDebug("<FxEditor> Updating fx parameter");

	switch (fxPrm) {
	case COL_D:
		curFx->color.d = value;
		colBuf = curFx->color;
		break;	
	case COL_R:
		curFx->color.r = value;
		colBuf = curFx->color;
		break;	
	case COL_G:
		curFx->color.g = value;
		colBuf = curFx->color;
		break;
	case COL_B:
		curFx->color.b = value;
		colBuf = curFx->color;
		break;

	case ATTRIBUTE:
		curFx->attribute = static_cast<Attribute>(static_cast<int>(value));
		colBuf = curFx->color;
		switch (curFx->attribute)
		{
		case DIM:
			rgbSetter = rgbSetter_D;
			break;
		case COLOR_R:
			rgbSetter = rgbSetter_R;
			break;
		case COLOR_G:
			rgbSetter = rgbSetter_G;
			break;
		case COLOR_B:
			rgbSetter = rgbSetter_B;
			break;
		}
		break;

	case WAVEFORM:
		curFx->form = static_cast<WaveFormType>(static_cast<int>(value));
		generateWave();
		break;
	case SPEED:
		curFx->bpm = value;
		calcSpeedIncr();
		break;
	case RATE:
		curFx->rate = value;
		calcSpeedIncr();
		break;
	case DIRECTION:
		curFx->direction = static_cast<Direction>(static_cast<int>(value));
		calcSpeedIncr();
		if (curFx->direction == BOUNCE_FWD || curFx->direction == BOUNCE_BCK)
			bounce = true;
		else
			bounce = false;
		break;
	case GROUPS:
		curFx->groups = value;
		phase.setGroups(value);
		break;
	case HIGH:
		curFx->high = value;
		generateWave();
		break;
	case LOW:
		curFx->low = value;
		generateWave();
		break;
	case DUTYCYCLE:
		curFx->dutycycle = value;
		generateWave();
		break;	
	case WINGS:
		curFx->wings = value;
		calcWings();
		break;
	}
	fileHandler->setEffect(*curFx);
}


void FxEditor::setEffectCol(RgbColor col)
{
	curFx->color = col;
}


Effect* FxEditor::getFxParameters() {
	return curFx;
}