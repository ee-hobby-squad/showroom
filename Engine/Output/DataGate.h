#pragma once

#include <thread> 
#include <memory> 

#include "../Utils/MutexedData.h"
#include "../Communication/CommunicationProvider.h"
#include "wx/log.h"

class DataGate
{
public:
	DataGate(MutexedData* dataAddr);
    virtual ~DataGate();

    std::thread mOutputThreadObject;
	void outputThread();

    void resize(uint32_t newSize);
    
    void setUDPCommunicationProvider();
    void setSerialCommunicationProvider();
private:
	CommunicationProvider *mCommunicationProvider;
    std::unique_ptr<CommunicationProvider> mSerialCP, mUDPCP;

	//MutexedData* leds = new MutexedData(NUM_LEDS);
	MutexedData* leds = nullptr;

    bool mRequestStopCommunicating = false;
    bool mIsCommunicating = false;

    void stopCommunication();
};

