#include "DataGate.h"

#include <chrono>

#include "../Communication/UDP/BufferedUDPServer.h"
#include "../Communication/UDP/UDPServer.h"
#if defined(__linux__)
#include "../Communication/UDP/UDPServerLinux.h"
#elif defined(_WIN32) || defined(_WIN64)
#include "../Communication/UDP/UDPServerWindows.h"
#endif
#include "../Communication/UDP/UDPCommunicationProvider.h"
#include "../Communication/Serial/SerialCommunicationProvider.h"
#include "../Config/Configuration.h"

BufferedUDPServer *bufferedServer;
UDPServer *udpServer;

// Constructor starts up the output thread 
DataGate::DataGate(MutexedData* dataAddr) {
#if defined(__linux__)
	udpServer = new UDPServerLinux;
#elif defined(_WIN32) || defined(_WIN64)
	udpServer = new UDPServerWindows;
#endif
    bufferedServer = new BufferedUDPServer(*udpServer);

    auto config = ConfigurationManager::loadConfig();
    if (config.communicationProvider == "UDP")
    {
        setUDPCommunicationProvider();
    }
    else if (config.communicationProvider == "Serial")
    {
        setSerialCommunicationProvider();
    }

	leds = dataAddr;

}

DataGate::~DataGate()
{
    stopCommunication();
    delete bufferedServer;
    delete udpServer;
}


// Output thread that sends data to Arduino
void DataGate::outputThread() {
    mIsCommunicating = true;
	wxLogDebug("<DataGate> Starting output thread");
	while (!mRequestStopCommunicating) {
		auto receive = mCommunicationProvider->recv();					// Wait for the Arduino to allow data

		if (receive.size() && receive[0] == 'A') {   
			// If Arduino sent permission ('A')
			std::vector<uint8_t> startAddr = leds->getData();			// Get the latest data from the memory

            // wxLogDebug("leds[0] = {%d, %d, %d}", startAddr[0], startAddr[1], startAddr[2]);
            // wxLogDebug("leds[1] = {%d, %d, %d}", startAddr[3], startAddr[4], startAddr[5]);

			mCommunicationProvider->send(startAddr);			// Write to COM using the pcSerial object
		}
	}
	wxLogDebug("<DataGate> Stopping output thread");
    mIsCommunicating = false;
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
}

void DataGate::setUDPCommunicationProvider()
{
    stopCommunication();

    auto config = ConfigurationManager::loadConfig();
    if (!udpServer->bind({ config.udpServerIp, config.udpServerPort })) return;

    if (config.udpClientIp == "unknown" || config.udpClientPort == 0) {
        mUDPCP = std::make_unique<UDPCommunicationProvider>(*bufferedServer);
    } else {
        mUDPCP = std::make_unique<UDPCommunicationProvider>(
                *bufferedServer,
                UDPSocketAddress{config.udpClientIp, config.udpClientPort}
        );
    }

    mCommunicationProvider = mUDPCP.get();

    mOutputThreadObject = std::thread(&DataGate::outputThread, this);
}

void DataGate::setSerialCommunicationProvider()
{
    stopCommunication();

    auto config = ConfigurationManager::loadConfig();
    mSerialCP = std::make_unique<SerialCommunicationProvider>(
            config.serialPort,
            config.serialBaudRate
    );

    mCommunicationProvider = mSerialCP.get();

    mOutputThreadObject = std::thread(&DataGate::outputThread, this);
}

void DataGate::stopCommunication()
{
    if (!mIsCommunicating) return;
    mRequestStopCommunicating = true;

    mCommunicationProvider->stop();

    // We know it will stop so we can detach from it to prevent it from exiting this thread
    mOutputThreadObject.detach();
    while (mIsCommunicating)
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    mRequestStopCommunicating = false;
}
