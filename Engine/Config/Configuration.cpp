#include "Configuration.h"

#include <fstream>
#include <sstream>
#include <utility>

#include "wx/log.h"

std::pair<std::string, std::string> readItem(std::istream &fileStream)
{
    std::string line;
    std::string key, value;

    // Read 1 full line
    std::getline(fileStream, line);

    if (line.size() == 0) return { "", "" };

    // Convert the line to a string stream for easier parsing
    std::stringstream input(line);

    // read the key
    input >> key;

    char next;

    // ignore any number of spaces
    while ((next = input.peek()) == ' ') input.ignore(1);

    if (next != '=')
    {
        wxLogDebug("<ConfigurationManager> Invalid line in config file");
        return { "invalid", "line" };
    }

    input.ignore(1);

    // ignore any number of spaces
    while ((next = input.peek()) == ' ') input.ignore(1);

    // Read the value
    input >> value;

    // Return the key value pair
    return { key, value };
}

ShowRoomConfig ConfigurationManager::loadFromFile(const std::string &filename)
{
    ShowRoomConfig config;

    std::ifstream inputFile(filename);

    if (!inputFile.is_open()) return config;

    while (!inputFile.eof())
    {
        auto [key, value] = readItem(inputFile);

        if (key == "led_count") config.numberOfLeds = std::stoul(value);
        if (key == "communication_provider") config.communicationProvider = value;
        if (key == "udp_server_ip") config.udpServerIp = value;
        if (key == "udp_server_port") config.udpServerPort = std::stoul(value);
        if (key == "udp_client_ip") config.udpClientIp = value;
        if (key == "udp_client_port") config.udpClientPort = std::stoul(value);
        if (key == "serial_port") config.serialPort = value;
        if (key == "serial_baud_rate") config.serialBaudRate = std::stoul(value);
    }

    return config;
}

void ConfigurationManager::saveToFile(const std::string &filename,
                                      const ShowRoomConfig &config)
{
    // std::ofstream::trunc will clear the file upon opening so we can fully
    // overwrite it
    std::ofstream outputFile(filename, std::ofstream::trunc);

    outputFile << "led_count = " << config.numberOfLeds << "\n";
    outputFile << "communication_provider = " << config.communicationProvider << "\n";
    outputFile << "udp_server_ip = " << config.udpServerIp << "\n";
    outputFile << "udp_server_port = " << config.udpServerPort << "\n";
    outputFile << "udp_client_ip = " << config.udpClientIp << "\n";
    outputFile << "udp_client_port = " << config.udpClientPort << "\n";
    outputFile << "serial_port = " << config.serialPort << "\n";
    outputFile << "serial_baud_rate = " << config.serialBaudRate << "\n";

    outputFile.flush();
    outputFile.close();
}

ShowRoomConfig ConfigurationManager::loadConfig()
{
    return ConfigurationManager::loadFromFile("userdata/showroom.cfg");
}
