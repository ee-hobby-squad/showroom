#pragma once

#include <cstdint>
#include <string>

struct ShowRoomConfig
{
    uint32_t numberOfLeds = 60;
    std::string communicationProvider = "UDP";
    std::string udpServerIp = "";
    uint16_t udpServerPort = 6603;

    std::string udpClientIp = "";
    uint16_t udpClientPort = 6600;

    std::string serialPort = "";
    uint32_t serialBaudRate = 115200;
};

class ConfigurationManager
{
public:
    static ShowRoomConfig loadConfig();
    static ShowRoomConfig loadFromFile(const std::string &filename);
    static void saveToFile(const std::string &filename,
                           const ShowRoomConfig &config);
};
