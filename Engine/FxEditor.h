
#include "FxRunner.h"
#include "DataHandling/EffectFileHandler.h"

#ifndef FXEDITOR_H
#define FXEDITOR_H

class FxEditor : public FxRunner
{
public:
	FxEditor(std::vector<uint8_t>* led, Effect* FX);
	~FxEditor();
	void setFxParameter(FxParameter fxPrm, float value);
	void setEffectCol(RgbColor col);
	Effect* getFxParameters();

private:
	EffectFileHandler* fileHandler = nullptr;
};

#endif