/*NAME:		MainEngine.h
* AUTHOR:	Yoran Staal
* 
* DESCRIPTION: The core of the control part of the software. The GUI sends all updates to 
* this class, and they will be handles here. The updates will be received intantaneously.
* There is also a seperate thread that takes care of frame updates (30 FPS). Dependend on
* the mode that the engine is in it will write the static RGB values or let the fxEngine 
* class take care of it.
*/

#pragma once

#include <string.h>
#include <array>
#include <thread>
#include <optional>

#include "Scripting/LuaEngine.h"
#include "Utils/MutexedData.h"
#include "Utils/EngineStructures.h"
#include "Config/Configuration.h"
#include "FxEditor.h"
#include "FxRunner.h"
#include "Utils/Effect.h"
#include "../Interface/wxIDs.h"

#include "wx/log.h"

// The different engine modes
enum Mode {staticMode , effectMode1, effectMode2};


class MainEngine
{
public:
	MainEngine(MutexedData &, const std::optional<std::string> &);
	~MainEngine();

	void inputBtnChange(uint32_t buttonID);							// Takes care of button value changes
	void inputSldChange(uint32_t sliderID, uint32_t value);			// Takes care of slider value changes
    void onSettingsChange();                                        // Called whenever the app settings change
	void toolBarInput(uint16_t id);							// Handles input from the toolbar
	void effectSelection(uint16_t id);
	FxEditor *newEffectEditor();									// Called when an effect editor window is opened

    // Used to add a runner
    // Automatically called when the effect editor is opened
	void addRunner(FxRunner* runner);

    // Used to remove a runner
    // Automatically called when the effect editor is closed
	void removeRunner(FxRunner* runner);

	std::vector<EffectInfo>* getEffectList();						// Called to get the complete list of effects

    inline std::vector<uint8_t> *getLeds() { return &leds; }

private:
	void update();											// Function that updates the RGB values
	void updateInterrupt();									// This function is a timer that wakes up the thread
	void effectUpdate();
	void setColorAll(RgbColor col);							// Function that updates all leds to one color
	void setColorSingle(uint16_t ledNr, RgbColor col);		// Function that updates one led to a color

    ShowRoomConfig mConfig;
    LuaEngine mLuaEngine;

	std::vector<uint8_t> leds;										// 'uint8_t array' that holds all RGB values within the class
	MutexedData& mRGBData;

	std::vector<FxRunner*> runningEffects;
	EffectFileHandler* fxFiles;

	std::vector<EffectInfo> allEffects;

	//Mode parameters
	bool fxEditorOpen = false;
	Mode mode = staticMode;
	RgbColor staticColor;
	RgbColor colorZero;
	uint8_t dim = 255;

	//Effect stuff
	uint16_t curFxEditId = 0;
	uint16_t curFxSelId = 1;
	Effect tempFx;			//TEMP

    bool mIsRunning = true;
    bool mCanClose = false;

    std::thread mLuaScriptThread;
};
