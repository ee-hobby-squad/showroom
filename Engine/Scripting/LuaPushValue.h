#pragma once

#include <string>
#include <vector>
#include "../Utils/EngineStructures.h"
#include "../Utils/Effect.h"

struct lua_State;

void pushValue(lua_State *state, int value);
void pushValue(lua_State *state, std::string value);
void pushValue(lua_State *state, float value);
void pushValue(lua_State *state, EffectInfo value);
void pushValue(lua_State *state, RgbColor value);
void pushValue(lua_State *state, Effect value);

void pushValue(lua_State *state, std::vector<int> value);
void pushValue(lua_State *state, std::vector<Effect> value);
void pushValue(lua_State *state, std::vector<EffectInfo> value);
