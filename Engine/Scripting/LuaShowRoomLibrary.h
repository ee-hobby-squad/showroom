#include "LuaEngine.h"

namespace LSRL // LuaShowRoomLib
{
    int getEffectsFunction(lua_State *l);
    int storeEffectFunction(lua_State *l);
    int startEffectFunction(lua_State *l);
    int stopEffectFunction(lua_State *l);
    int sleepFunction(lua_State *l);
}
