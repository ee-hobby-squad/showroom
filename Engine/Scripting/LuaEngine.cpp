#include "LuaEngine.h"

extern "C"
{
    #include "lauxlib.h"
    #include "lualib.h"
}

#include "LuaGetValue.h"

void LuaEngine::initLua()
{
    // If we are already initialized just leave
    if (isInitialized()) return;

    // Initialize a new lua main thread
    mLuaState = luaL_newstate();

    // Open the lua standard libraries
    luaL_openlibs(mLuaState);
}

void LuaEngine::destroyLua()
{
    // Close the lua main thread
    lua_close(mLuaState);
    mLuaState = nullptr;
}

bool LuaEngine::isInitialized() const
{
    if (mLuaState == nullptr) return false;

    // Get the status of the thread pointer
    int luaStatus = lua_status(mLuaState);
    // If the thread is running or suspended lua is initialized
    return luaStatus == LUA_OK || luaStatus == LUA_YIELD;
}

void LuaEngine::exposeFunction(lua_CFunction f,
                               const std::string &name, void *ud)
{
    lua_pushlightuserdata(mLuaState, ud);
    lua_pushcclosure(mLuaState, f, 1);
    lua_setglobal(mLuaState, name.c_str());
}

void LuaEngine::runString(const std::string &code)
{
    int s;

    s = luaL_loadstring(mLuaState, code.c_str());
    if (s == LUA_OK)
    {
        s = lua_pcall(mLuaState, 0, LUA_MULTRET, 0);
        if (s == LUA_OK)
        {
            lua_pop(mLuaState, lua_gettop(mLuaState));
        }
    }
}

void LuaEngine::runFile(const std::string &path)
{
    int s;
    s = luaL_loadfile(mLuaState, path.c_str());
    if (s == LUA_OK)
    {
        s = lua_pcall(mLuaState, 0, LUA_MULTRET, 0);
        if (s == LUA_OK)
        {
            lua_pop(mLuaState, lua_gettop(mLuaState));
        }
    }
}

int LuaEngine::getGlobalInt(const std::string &name)
{
    lua_getglobal(mLuaState, name.c_str());

    return getValue<int>(mLuaState, lua_gettop(mLuaState));
}

lua_State *LuaEngine::getState() const
{
    return mLuaState;
}
