#pragma once

extern "C"
{
    #include "lua.h"
}

#include <string>
#include <stdexcept>
#include <sstream>
#include "LuaPushValue.h"
#include "LuaGetValue.h"

class LuaEngine
{
    private:
        lua_State *mLuaState = nullptr;
    public:
        LuaEngine() = default;
        ~LuaEngine() = default;

        void initLua();
        void destroyLua();

        bool isInitialized() const;

        void exposeFunction(lua_CFunction f, const std::string &name, void *ud);
        void runString(const std::string &code);
        void runFile(const std::string &path);
        int getGlobalInt(const std::string &name);
        lua_State *getState() const;
};
