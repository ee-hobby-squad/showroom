#include "LuaPushValue.h"
#include "LuaEngine.h"

template<typename T>
void setTable(lua_State *state, const std::string &k, T value, int index)
{
    pushValue(state, k);
    pushValue(state, value);
    lua_settable(state, index);
}

void pushValue(lua_State *state, int value)
{
    lua_pushinteger(state, value);
}

void pushValue(lua_State *state, std::string value)
{
    lua_pushstring(state, value.c_str());
}

void pushValue(lua_State *state, float value)
{
    lua_pushnumber(state, value);
}

void pushValue(lua_State *state, EffectInfo value)
{
    lua_newtable(state);
    int tableIndex = lua_gettop(state);

    setTable<int>(state, "id", value.id, tableIndex);
    setTable(state, "name", value.name, tableIndex);
}

void pushValue(lua_State *state, RgbColor value)
{
    lua_newtable(state);
    int tableIndex = lua_gettop(state);

    setTable<int>(state, "r", value.r, tableIndex);
    setTable<int>(state, "g", value.g, tableIndex);
    setTable<int>(state, "b", value.b, tableIndex);
    setTable<int>(state, "d", value.d, tableIndex);
}

void pushValue(lua_State *state, Effect value)
{
    lua_newtable(state);
    int tableIndex = lua_gettop(state);

    setTable(state, "info", value.info, tableIndex);
    setTable<int>(state, "attribute", value.attribute, tableIndex);
    setTable<int>(state, "form", value.form, tableIndex);
    setTable<int>(state, "direction", value.direction, tableIndex);
    setTable(state, "bpm", value.bpm, tableIndex);
    setTable(state, "rate", value.rate, tableIndex);
    setTable<int>(state, "phase", value.phase, tableIndex);
    setTable<int>(state, "high", value.high, tableIndex);
    setTable<int>(state, "low", value.low, tableIndex);
    setTable<int>(state, "dutycycle", value.dutycycle, tableIndex);
    setTable<int>(state, "groups", value.groups, tableIndex);
    setTable<int>(state, "blocks", value.blocks, tableIndex);
    setTable<int>(state, "wings", value.wings, tableIndex);
    setTable(state, "color", value.color, tableIndex);
}

void pushValue(lua_State *state, std::vector<int> value)
{
    lua_newtable(state);
    int tableIndex = lua_gettop(state);

    for (size_t index = 1; index < value.size() + 1; ++index)
    {
        pushValue(state, value[index - 1]);
        lua_seti(state, tableIndex, index);
    }
}

void pushValue(lua_State *state, std::vector<EffectInfo> value)
{
    lua_newtable(state);
    int tableIndex = lua_gettop(state);

    for (size_t index = 1; index < value.size() + 1; ++index)
    {
        pushValue(state, value[index - 1]);
        lua_seti(state, tableIndex, index);
    }
}

void pushValue(lua_State *state, std::vector<Effect> value)
{
    lua_newtable(state);
    int tableIndex = lua_gettop(state);

    for (size_t index = 1; index < value.size() + 1; ++index)
    {
        pushValue(state, value[index - 1]);
        lua_seti(state, tableIndex, index);
    }
}
