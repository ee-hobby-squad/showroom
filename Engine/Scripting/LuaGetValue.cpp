#include "LuaGetValue.h"
#include "LuaPushValue.h"
#include "LuaEngine.h"

#include "../Utils/Effect.h"
#include "../Utils/EngineStructures.h"

template<typename T>
T getTable(lua_State *state, const std::string &key, int index)
{
    pushValue(state, key);
    lua_gettable(state, index);
    T val = getValue<T>(state, lua_gettop(state));
    return val;
}

template<>
int getValue(lua_State *state, int index)
{
    int suc;
    int val = lua_tointegerx(state, index, &suc);
    return val;
}

template<>
float getValue(lua_State *state, int index)
{
    int suc;
    float val = lua_tonumberx(state, index, &suc);
    return val;
}

template<>
std::string getValue(lua_State *state, int index)
{
    size_t num;
    const char *val = lua_tolstring(state, index, &num);
    return std::string(val, num);
}

template<>
Attribute getValue(lua_State *state, int index)
{
    return static_cast<Attribute>(getValue<int>(state, index));
}

template<>
WaveFormType getValue(lua_State *state, int index)
{
    return static_cast<WaveFormType>(getValue<int>(state, index));
}

template<>
Direction getValue(lua_State *state, int index)
{
    return static_cast<Direction>(getValue<int>(state, index));
}

template<>
EffectInfo getValue(lua_State *state, int index)
{
    EffectInfo ei;
    ei.id = getTable<int>(state, "id", index);
    ei.name = getTable<std::string>(state, "name", index);
    return ei;
}

template<>
RgbColor getValue(lua_State *state, int index)
{
    RgbColor col;
    col.r = getTable<int>(state, "r", index);
    col.g = getTable<int>(state, "g", index);
    col.b = getTable<int>(state, "b", index);
    col.d = getTable<int>(state, "d", index);
    return col;
}

template<>
Effect getValue(lua_State *state, int index)
{
    Effect e;
    e.info = getTable<EffectInfo>(state, "info", index);
    e.attribute = getTable<Attribute>(state, "attribute", index);
    e.form = getTable<WaveFormType>(state, "form", index);
    e.direction = getTable<Direction>(state, "direction", index);
    e.bpm = getTable<float>(state, "bpm", index);
    e.rate = getTable<float>(state, "rate", index);
    e.phase = getTable<int>(state, "phase", index);
    e.high = getTable<int>(state, "high", index);
    e.low = getTable<int>(state, "low", index);
    e.dutycycle = getTable<int>(state, "dutycycle", index);
    e.groups = getTable<int>(state, "groups", index);
    e.blocks = getTable<int>(state, "blocks", index);
    e.wings = getTable<int>(state, "wings", index);
    e.color = getTable<RgbColor>(state, "color", index);

    return e;
}
