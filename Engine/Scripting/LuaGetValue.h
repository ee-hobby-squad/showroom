#pragma once

struct lua_State;

template<typename T>
T getValue(lua_State *state, int index);
