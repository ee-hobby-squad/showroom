#pragma once

#include "../MainEngine.h"
#include "LuaEngine.h"

#include "LuaShowRoomLibrary.h"

namespace LFL // LuaFunctionLoader
{
    inline void loadFunctions(LuaEngine &engine, MainEngine &mainEngine)
    {
        using namespace LSRL;
        engine.exposeFunction(getEffectsFunction, "getEffects", &mainEngine);
        engine.exposeFunction(storeEffectFunction, "storeEffect", nullptr);
        engine.exposeFunction(startEffectFunction, "startEffect", &mainEngine);
        engine.exposeFunction(stopEffectFunction, "stopEffect", &mainEngine);
        engine.exposeFunction(sleepFunction, "sleep", nullptr);
    }
}
