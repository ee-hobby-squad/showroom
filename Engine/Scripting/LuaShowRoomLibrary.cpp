#include "../MainEngine.h"

#include <vector>
#include <stdexcept>
#include <thread>
#include <chrono>

namespace
{
    EffectFileHandler fileHandler;
    FxRunner *luaFxRunner = nullptr;
    Effect *runningEffect = nullptr;
}

namespace LSRL // LuaShowRoomLib
{
    int getEffectsFunction(lua_State *l)
    {
        void *ud = lua_touserdata(l, lua_upvalueindex(1));
        MainEngine &engine = *static_cast<MainEngine*>(ud);

        try
        {
            std::vector<Effect> effects{};
            for (auto &effect: *engine.getEffectList())
            {
                effects.push_back(fileHandler.getEffect(effect.id));
            }
            pushValue(l, effects);
        }
        catch (std::exception &e)
        {
            std::cerr << e.what() << std::endl;
            return 0;
        }

        return 1;
    }

    int storeEffectFunction(lua_State *l)
    {
        if (lua_gettop(l) != 1 || !lua_istable(l, 1))
        {
            std::cout << "storeEffect: wrong argument" << std::endl;
            return 0;
        }

        Effect e = getValue<Effect>(l, 1);
        fileHandler.setEffect(e);


        return 1;
    }

    int startEffectFunction(lua_State *l)
    {
        void *ud = lua_touserdata(l, lua_upvalueindex(1));
        MainEngine &engine = *static_cast<MainEngine*>(ud);

        if (lua_gettop(l) != 1 || !lua_istable(l, 1))
        {
            std::cout << "startEffect: wrong argument" << std::endl;
            return 0;
        }

        Effect e = getValue<Effect>(l, 1);

        // Prevent memory leaks
        if (luaFxRunner) {
            engine.removeRunner(luaFxRunner);
            delete luaFxRunner;
            luaFxRunner = nullptr;
        }
        if (runningEffect) {
            delete runningEffect;
            runningEffect = nullptr;
        }

        runningEffect = new Effect(e);
        luaFxRunner = new FxRunner(engine.getLeds(), runningEffect);
        engine.addRunner(luaFxRunner);

        return 1;
    }

    int stopEffectFunction(lua_State *l)
    {
        void *ud = lua_touserdata(l, lua_upvalueindex(1));
        MainEngine &engine = *static_cast<MainEngine*>(ud);

        if (luaFxRunner) {
            engine.removeRunner(luaFxRunner);
            delete luaFxRunner;
            luaFxRunner = nullptr;
        }
        if (runningEffect) {
            delete runningEffect;
            runningEffect = nullptr;
        }

        return 1;
    }

    int sleepFunction(lua_State *l)
    {
        if (lua_gettop(l) != 1 || !lua_isinteger(l, 1))
        {
            std::cout << "sleep: wrong argument" << std::endl;
            return 0;
        }

        int time = getValue<int>(l, 1);
        std::this_thread::sleep_for(std::chrono::milliseconds(time));

        return 1;
    }
}
