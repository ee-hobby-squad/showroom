#pragma once

#include <vector>
#include <cstdint>

class CommunicationProvider
{
    public:
        virtual ~CommunicationProvider() {}
        using DataType = std::vector<uint8_t>;
        virtual void send(const DataType &data) const = 0;
        virtual DataType recv() const = 0;

        virtual void stop() = 0;
};
