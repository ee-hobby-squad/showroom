#pragma once

#include "../CommunicationProvider.h"

#include <string>

class SerialCommunicationProvider : public CommunicationProvider
{
    private:
        std::string mPort;
        int mBaudRate;
#if defined(__linux__)
        mutable int mSerialFD = -1;
#endif
    public:
        SerialCommunicationProvider(const std::string &port, int baudRate);
        ~SerialCommunicationProvider();

        void send(const CommunicationProvider::DataType &data) const override;
        CommunicationProvider::DataType recv() const override;

        void close();
        void stop() override {}
};
