#include "../Serial/SerialCommunicationProvider.h"

#include "wx/log.h"
#if defined(__linux__)
#include <thread>
#include <chrono>

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <termios.h>
#include <string.h>
#elif defined(_WIN32) || defined(_WIN64)
#include <windows.h>
#endif

SerialCommunicationProvider::SerialCommunicationProvider(
        const std::string &port,
        int baudRate)
    : mPort(port), mBaudRate(baudRate)
{
}

#if defined(__linux__)
int setupCommPort(const std::string &port, int baudRate)
{
    int serialFD = open(port.c_str(), O_RDWR);
    if (serialFD < 0)
    {
        return -1;
    }

    termios tty;

    if (tcgetattr(serialFD, &tty) != 0)
    {
        return -1;
    }

    tty.c_cflag &=                 ~PARENB; // no parity
    tty.c_cflag &=                 ~CSTOPB; // 1 stop bit
    tty.c_cflag |=                     CS8; // 8 bits per byte
    tty.c_cflag &=                ~CRTSCTS; // Disable flow control
    tty.c_cflag |=          CREAD | CLOCAL; // Dunno what this does,
                                            // but it is reccomended
                                            // so i'll add it

    tty.c_lflag &=                 ~ICANON; // Disable canonical mode
    tty.c_lflag &=                   ~ECHO; // Disable echo
    tty.c_lflag &=                  ~ECHOE; // Disable erasure
    tty.c_lflag &=                 ~ECHONL; // Disable new-line echo

    tty.c_lflag &=                   ~ISIG; // Disable signal chars
    tty.c_iflag &= ~(IXON | IXOFF | IXANY); // Turn off s/w flow ctrl
    tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK
                     |ISTRIP|INLCR|IGNCR
                                   |ICRNL); // Disable any special handling
                                            // of received bytes

    // No output handling just send the raw data
    tty.c_oflag &= ~OPOST;
    tty.c_oflag &= ~ONLCR;

    tty.c_cc[VTIME] = 0; // No timeout
    tty.c_cc[VMIN]  = 1; // Read 1 byte

    cfsetispeed(&tty, baudRate);
    cfsetospeed(&tty, baudRate);

    // Save tty settings, also checking for error
    if (tcsetattr(serialFD, TCSANOW, &tty) != 0)
    {
        return -1;
    }

    return serialFD;
}
#endif

SerialCommunicationProvider::~SerialCommunicationProvider()
{
#if defined(__linux__)
    close();
#endif
}

void SerialCommunicationProvider::send(const CommunicationProvider::DataType &data) const
{
#if defined(__linux__)
    if (mSerialFD < 0)
    {
        if ((mSerialFD = setupCommPort(mPort, mBaudRate)) == -1) return;
    }

    write(mSerialFD, data.data(), data.size());
#elif defined(_WIN32) || defined(_WIN64)
    DCB dcb;
    DWORD byteswritten;
    LPCWSTR portSpec = (LPCWSTR)mPort.c_str();
    HANDLE hPort = CreateFile(
        portSpec,
        GENERIC_WRITE,
        0,
        NULL,
        OPEN_EXISTING,
        0,
        NULL
    );

    if (!GetCommState(hPort, &dcb))
        return;

    dcb.BaudRate = mBaudRate;	//Baud
    dcb.ByteSize = 8;			//8 data bits
    dcb.Parity = NOPARITY;		//no parity
    dcb.StopBits = ONESTOPBIT;	//1 stop

    if (!SetCommState(hPort, &dcb))
        return;

    WriteFile(hPort, data.data(), data.size(), &byteswritten, NULL);

    bool retVal = true;
    CloseHandle(hPort); //close the handle
#endif
}

std::vector<uint8_t> SerialCommunicationProvider::recv() const
{
#if defined(__linux__)
    if (mSerialFD < 0)
    {
        if ((mSerialFD = setupCommPort(mPort, mBaudRate)) == -1)
        {
            // Wait a little to prevent spam trying to open the serial com
            std::this_thread::sleep_for(std::chrono::seconds(1));
            return {};
        }
    }

    uint8_t data;
    int n = read(mSerialFD, &data, 1);

    if (n > 0)
    {
        return { data };
    }
#elif defined(_WIN32) || defined(_WIN64)
    DCB dcb;
    uint8_t retVal;
    BYTE Byte = 0;
    DWORD dwBytesTransferred;
    DWORD dwCommModemStatus;
    LPCWSTR portSpec = (LPCWSTR)mPort.c_str();
    HANDLE hPort = CreateFile(
        portSpec,
        GENERIC_READ,
        0,
        NULL,
        OPEN_EXISTING,
        0,
        NULL
    );

    if (!GetCommState(hPort, &dcb))
    {
        wxLogDebug("ERROR GetCommState : %s", mPort.c_str());
        return {};
    }

    dcb.BaudRate = mBaudRate;	//Baud
    dcb.ByteSize = 8;			//8 data bits
    dcb.Parity = NOPARITY;		//no parity
    dcb.StopBits = ONESTOPBIT;	//1 stop

    if (!SetCommState(hPort, &dcb))
    {
        wxLogDebug("ERROR SetCommState");
        return {};
    }

    SetCommMask(hPort, EV_RXCHAR | EV_ERR);			//receive character event
    WaitCommEvent(hPort, &dwCommModemStatus, 0);	//wait for character

    if (dwCommModemStatus & EV_RXCHAR)
        ReadFile(hPort, &Byte, 1, &dwBytesTransferred, 0);	//read 1

    else if (dwCommModemStatus & EV_ERR)
        retVal = 0x101;

    retVal = Byte;
    CloseHandle(hPort);

    return { retVal };
#endif
    return {};
}

void SerialCommunicationProvider::close()
{
#if defined(__linux__)
    if (mSerialFD > 0)
        ::close(mSerialFD);
#endif
}
