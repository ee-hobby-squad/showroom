#pragma once

#include <string>
#include <vector>
#include <utility>
#include <cstdint>

struct UDPSocketAddress
{
    std::string host;
    uint16_t port;

    bool operator==(const UDPSocketAddress &) const;
    bool operator<(const UDPSocketAddress &) const;
};

class UDPServer
{
public:
    virtual ~UDPServer() {}

    virtual bool bind(const UDPSocketAddress &address) = 0;
    virtual void close() = 0;
    virtual std::pair<UDPSocketAddress, std::vector<uint8_t>> read() const = 0;
    virtual void send(const UDPSocketAddress &,
                      const std::vector<uint8_t> &) const = 0;
};
