#include "UDPServerLinux.h"

// Linux headers
#if defined(__linux__)
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>

// Standard library headers
#include <cstring>
#include <cstdio>

addrinfo getHints()
{
    addrinfo hints;

    std::memset(&hints, 0, sizeof(hints));
    hints.ai_family   = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_flags    = 0;
    hints.ai_protocol = 0;

    return hints;
}

UDPSocketAddress serializeSocketAddress(sockaddr *addr, socklen_t len)
{
    char hostBuffer[NI_MAXHOST], serviceBuffer[NI_MAXSERV];

    getnameinfo(addr, len, hostBuffer, NI_MAXHOST, serviceBuffer, NI_MAXSERV,
                NI_NUMERICHOST | NI_NUMERICSERV);

    UDPSocketAddress returnValue;
    returnValue.host = std::string{hostBuffer};
    returnValue.port = std::stoul(std::string{serviceBuffer});

    return returnValue;
}

UDPServerLinux::UDPServerLinux()
    : mFileDescriptor(-1), mBoundToPort(false)
{}

UDPServerLinux::~UDPServerLinux()
{
    close();
}

bool UDPServerLinux::bind(const UDPSocketAddress &address)
{
    if (mBoundToPort) return true;

    if (address.host != "")
    {
        addrinfo hints = getHints(), *result, *itr;

        int success = getaddrinfo(address.host.c_str(),
                                  std::to_string(address.port).c_str(),
                                  &hints, &result);
        if (success != 0)
        {
            return false;
        }

        for (itr = result; itr != nullptr; itr = itr->ai_next)
        {
            mFileDescriptor = socket(itr->ai_family, itr->ai_socktype,
                                     itr->ai_protocol);
            if (mFileDescriptor == -1)
                continue;

            if (::bind(mFileDescriptor, itr->ai_addr, itr->ai_addrlen) == 0)
                break; // Success

            ::close(mFileDescriptor);
        }

        freeaddrinfo(result);

        if (itr == nullptr)
        {
            return false;
        }
    }
    else
    {
        sockaddr_in internetAddress;
        internetAddress.sin_family = AF_INET;
        internetAddress.sin_addr.s_addr = htonl(INADDR_ANY);
        internetAddress.sin_port = htons(address.port);
        mFileDescriptor = socket(AF_INET, SOCK_DGRAM, 0);
        int r = ::bind(mFileDescriptor,
                       (sockaddr*)&internetAddress, sizeof(internetAddress));
        if (r != 0) return false;
    }
    
    mBoundToPort = true;
    return true;
}

void UDPServerLinux::close()
{
    if (!mBoundToPort) return;

    shutdown(mFileDescriptor, SHUT_RDWR);
    ::close(mFileDescriptor);
    mBoundToPort = false;
}

std::pair<UDPSocketAddress, std::vector<uint8_t>> UDPServerLinux::read() const
{
    if (!mBoundToPort)
        return {{"",0}, {}};

    sockaddr_in clientAddress;
    unsigned int clientAddressLength = sizeof(clientAddress);

    uint8_t buffer[64 * 1024];

    int len = recvfrom(mFileDescriptor, (char*)buffer, 64 * 1024, MSG_WAITALL,
                       (sockaddr *)&clientAddress, &clientAddressLength);

    if (len <= 0) return {{"", 0}, {}};
    return {
        serializeSocketAddress((sockaddr *)&clientAddress, clientAddressLength),
        {buffer, buffer + len}
    };
}

void UDPServerLinux::send(const UDPSocketAddress &address,
                          const std::vector<uint8_t> &data) const
{
    if (!mBoundToPort) return;

    addrinfo hints = getHints(), *res;
    getaddrinfo(address.host.c_str(), std::to_string(address.port).c_str(),
                &hints, &res);

    sendto(mFileDescriptor, data.data(), data.size(), MSG_CONFIRM,
           res->ai_addr, res->ai_addrlen);
}

int UDPServerLinux::getFileDescriptor() const
{
    if (mBoundToPort)
        return mFileDescriptor;
    return -1;
}
#endif
