#include "UDPServerWindows.h"

#if defined(_WIN32) || defined(_WIN64)

namespace
{
    bool winSockIsInitialized = false;
    WSADATA wsaData;

    void initWinSock()
    {
        if (winSockIsInitialized) return;
        int res = WSAStartup(MAKEWORD(2, 2), &wsaData);
        if (res != NO_ERROR) return;
        winSockIsInitialized = true;
    }

    UDPSocketAddress serializeSocketAddress(SOCKADDR *addr, socklen_t len)
    {
        char hostBuffer[NI_MAXHOST], serviceBuffer[NI_MAXSERV];

        getnameinfo(addr, len, hostBuffer, NI_MAXHOST, serviceBuffer, NI_MAXSERV,
            NI_NUMERICHOST | NI_NUMERICSERV);

        UDPSocketAddress returnValue;
        returnValue.host = std::string{ hostBuffer };
        returnValue.port = std::stoul(std::string{ serviceBuffer });

        return returnValue;
    }
}

UDPServerWindows::UDPServerWindows()
{
    initWinSock();
}

UDPServerWindows::~UDPServerWindows()
{
}

bool UDPServerWindows::bind(const UDPSocketAddress& address)
{
    mSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (mSocket == INVALID_SOCKET) return false;
    sockaddr_in serverAddr;
    serverAddr.sin_family = AF_INET;
    serverAddr.sin_port = htons(address.port);
    inet_pton(AF_INET, address.host.c_str(), &serverAddr.sin_addr);

    if (::bind(mSocket, (SOCKADDR*)&serverAddr, sizeof(serverAddr))) return false;

    return true;
}

void UDPServerWindows::close()
{
    closesocket(mSocket);
}

std::pair<UDPSocketAddress, std::vector<uint8_t>> UDPServerWindows::read() const
{
    if (mSocket == INVALID_SOCKET) return { {"",0}, {} };
    
    sockaddr_in clientAddress;
    int clientAddressLength = sizeof(clientAddress);

    uint8_t buffer[64 * 1024];

    int len = recvfrom(mSocket, (char*)buffer, 64 * 1024, 0,
        (SOCKADDR *)&clientAddress, &clientAddressLength);

    if (len == SOCKET_ERROR) return { {"",0}, {} };

    return {
        serializeSocketAddress((sockaddr*)&clientAddress, clientAddressLength),
        {buffer, buffer + len}
    };
}

void UDPServerWindows::send(const UDPSocketAddress &address, const std::vector<uint8_t> &data) const
{
    if (mSocket == INVALID_SOCKET) return;

    sockaddr_in clientAddr;
    clientAddr.sin_family = AF_INET;
    clientAddr.sin_port = htons(address.port);
    inet_pton(AF_INET, address.host.c_str(), &clientAddr.sin_addr);

    int clientAddrSize = sizeof(clientAddr);


    sendto(mSocket, (const char *)data.data(), data.size(), 0,
        (SOCKADDR *)&clientAddr, clientAddrSize);

}
#endif