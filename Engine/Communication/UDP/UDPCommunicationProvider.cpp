#include "UDPCommunicationProvider.h"

UDPCommunicationProvider::UDPCommunicationProvider(BufferedUDPServer &server)
    : mServer(server)
{
    mAddress = mServer.recvFromUnknown();
    mServer.start();
}

UDPCommunicationProvider::UDPCommunicationProvider(BufferedUDPServer &server,
                                                   const UDPSocketAddress &addr)
    : mServer(server), mAddress(addr)
{
    mServer.start();
}


void UDPCommunicationProvider::
                send(const CommunicationProvider::DataType &data) const
{
    mServer.sendTo(mAddress, data);
}

CommunicationProvider::DataType UDPCommunicationProvider::recv() const
{
    return mServer.recvFrom(mAddress);
}

void UDPCommunicationProvider::stop()
{
    mServer.stop();
}
