#pragma once

#include "UDPServer.h"
#if defined(_WIN32) || defined(_WIN64)
#include <WinSock2.h>
#include <WS2tcpip.h>
#pragma comment(lib, "Ws2_32.lib")

class UDPServerWindows : public UDPServer
{
private:
    SOCKET mSocket = INVALID_SOCKET;
public:
    UDPServerWindows();
    ~UDPServerWindows();
    bool bind(const UDPSocketAddress& address) override;
    void close();
    std::pair<UDPSocketAddress, std::vector<uint8_t>> read() const override;
    void send(const UDPSocketAddress&,
        const std::vector<uint8_t>&) const override;
};
#endif