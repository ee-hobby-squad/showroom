#include "BufferedUDPServer.h"

#include "wx/log.h"
#include <functional>

BufferedUDPServer::BufferedUDPServer(UDPServer &server)
    : mServer(server), mReading(false)
{
}

BufferedUDPServer::~BufferedUDPServer()
{
    stop();
}

void BufferedUDPServer::readerFunc()
{
    while (mReading)
    {
        auto ret = mServer.read();

        if (ret.first == UDPSocketAddress{"", 0} && ret.second == std::vector<uint8_t>{})
        {
            mReading = false;
            mDataQueuesCV.notify_all();
            wxLogDebug("<BufferedUDPServer> Reading error cancelling thread");
            break;
        }

        {
            std::lock_guard<std::mutex> lk(mDataQueuesMutex);
            mDataQueues[ret.first].push(ret.second);
        }
        mDataQueuesCV.notify_all();
    }
}

void BufferedUDPServer::sendTo(const UDPSocketAddress &addr,
                               const std::vector<uint8_t> &data)
{
    mServer.send(addr, data);
}

UDPSocketAddress BufferedUDPServer::recvFromUnknown()
{
    if (mReading) return {};

    UDPSocketAddress ret;
    while ((ret = mServer.read().first) == UDPSocketAddress{"", 0});

    return ret;
}

std::vector<uint8_t> BufferedUDPServer::recvFrom(const UDPSocketAddress &address)
{
    std::unique_lock<std::mutex> lk(mDataQueuesMutex);

    if (mDataQueues[address].size() > 0)
    {
        auto val = mDataQueues[address].front();
        mDataQueues[address].pop();
        return val;
    }

    mDataQueuesCV.wait(lk, [&address, this]{
        return mDataQueues[address].size() > 0 || !mReading;
    });

    if (!mReading)
    {
        return {};
    }

    auto val = mDataQueues[address].front();
    mDataQueues[address].pop();
    return val;
}

void BufferedUDPServer::start()
{
    if (mReading) return;
    mReading = true;
    mReaderThread = std::thread{std::bind(&BufferedUDPServer::readerFunc, this)};
}

void BufferedUDPServer::stop()
{
    mServer.close();
    mReading = false;
    if (mReaderThread.joinable()) mReaderThread.join();
}
