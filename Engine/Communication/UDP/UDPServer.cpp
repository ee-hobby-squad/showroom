#include "UDPServer.h"

#if defined(__linux__)
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <cstring>
#include <string>
#endif

bool UDPSocketAddress::operator==(const UDPSocketAddress &o) const
{
#if defined(__linux__)
    addrinfo hints, *res1, *res2;
    std::memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;

    int suc = getaddrinfo(host.c_str(), std::to_string(port).c_str(),
                          &hints, &res1);
    if (suc != 0) return o.host == host && o.port == port;
    suc = getaddrinfo(o.host.c_str(), std::to_string(o.port).c_str(),
                      &hints, &res2);
    if (suc != 0) return o.host == host && o.port == port;


    uint32_t a1 = ((sockaddr_in*)res1->ai_addr)->sin_addr.s_addr;
    uint32_t a2 = ((sockaddr_in*)res2->ai_addr)->sin_addr.s_addr;

    freeaddrinfo(res1);
    freeaddrinfo(res2);

    return a1 == a2 && port == o.port;
#else
    return o.host == host && o.port == port;
#endif
}

bool UDPSocketAddress::operator<(const UDPSocketAddress &o) const
{
#if defined(__linux__)
    addrinfo hints, *res1, *res2;
    std::memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;

    int suc = getaddrinfo(host.c_str(), std::to_string(port).c_str(),
                          &hints, &res1);
    if (suc != 0) return false;
    suc = getaddrinfo(o.host.c_str(), std::to_string(o.port).c_str(),
                      &hints, &res2);
    if (suc != 0) return false;

    uint32_t a1 = ((sockaddr_in*)res1->ai_addr)->sin_addr.s_addr;
    uint32_t a2 = ((sockaddr_in*)res2->ai_addr)->sin_addr.s_addr;

    uint64_t conc1 = ((uint64_t)a1 << 32) | port;
    uint64_t conc2 = ((uint64_t)a2 << 32) | o.port;

    freeaddrinfo(res1);
    freeaddrinfo(res2);

    return conc1 < conc2;
#else
    return o.port < port;
#endif
}
