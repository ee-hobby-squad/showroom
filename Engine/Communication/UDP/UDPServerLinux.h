#pragma once

#include "UDPServer.h"
#if defined(__linux__)
class UDPServerLinux : public UDPServer
{
    private:
        int mFileDescriptor;
        bool mBoundToPort;
    public:
        UDPServerLinux();
        ~UDPServerLinux();
        bool bind(const UDPSocketAddress &address) override;
        void close();
        std::pair<UDPSocketAddress, std::vector<uint8_t>> read() const override;
        void send(const UDPSocketAddress &,
                  const std::vector<uint8_t> &) const override;

        int getFileDescriptor() const;
};
#endif