// Keep track of known clients
// buffer data for each client (1 FIFO buffer per client)
// send to a specific client and read from a clients buffer
#pragma once

#include "UDPServer.h"

#include <queue>
#include <map>
#include <thread>
#include <condition_variable>
#include <mutex>

class BufferedUDPServer
{
private:
    using DataQueue = std::queue<std::vector<uint8_t>>;

    UDPServer &mServer;

    std::map<UDPSocketAddress, DataQueue> mDataQueues;

    std::mutex mDataQueuesMutex;
    std::condition_variable mDataQueuesCV;

    std::thread mReaderThread;

    bool mReading;
    virtual void readerFunc();
public:
    BufferedUDPServer(UDPServer &server);
    virtual ~BufferedUDPServer();
    virtual void sendTo(const UDPSocketAddress &addr,
            const std::vector<uint8_t> &data);
    virtual UDPSocketAddress recvFromUnknown();
    virtual std::vector<uint8_t> recvFrom(const UDPSocketAddress &address);

    void start();
    virtual void stop();
};
