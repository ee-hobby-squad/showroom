#pragma once

#include "../CommunicationProvider.h"
#include "BufferedUDPServer.h"

class UDPCommunicationProvider : public CommunicationProvider
{
    private:
        BufferedUDPServer &mServer;
        UDPSocketAddress mAddress;
    public:
        UDPCommunicationProvider(BufferedUDPServer&);
        UDPCommunicationProvider(BufferedUDPServer&, const UDPSocketAddress &);
        void send(const CommunicationProvider::DataType &data) const override;
        CommunicationProvider::DataType recv() const override;

        void stop() override;
};
