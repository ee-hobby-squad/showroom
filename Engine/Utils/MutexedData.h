#ifndef Included_MutexedData
#define Included_MutexedData

#include <string>
#include <vector>
#include <mutex>

#include "wx/log.h"


class MutexedData
{
private:
	std::mutex mtx;
	uint32_t dataSize;
public:
	std::vector<uint8_t> data;

	MutexedData(uint32_t dataSize)
        : dataSize(dataSize)
    {
		wxLogDebug("<Mutexed data> Initializing mutexed data");
		data.reserve(dataSize);

		for (uint32_t i = 0; i < dataSize; i++) {
			data.push_back(0x00);
		}
	}

	~MutexedData() {

	}

	std::vector<uint8_t> getData() {
		mtx.lock();
		std::vector<uint8_t> dataOut = data;
		mtx.unlock();
		return dataOut;
	}

	bool setData(const std::vector<uint8_t> &newData) {
		if (newData.size() == dataSize) {
			mtx.lock();
			data = newData;
			mtx.unlock();
			return true;
		}
		else {
			return false;
		}
	}

    void resize(uint32_t newSize)
    {
		wxLogDebug("<Mutexed data> Resizing mutexed data %u -> %u",
                   dataSize,
                   newSize);

        // aquire a mutex lock
        std::lock_guard<std::mutex> lock(mtx);

        // Set the new size
        dataSize = newSize;
        data.resize(newSize);

        // Free up some memory
        data.shrink_to_fit();
    }
};

#endif
