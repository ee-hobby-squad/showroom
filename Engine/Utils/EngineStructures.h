#pragma once
#include <cstdint>

struct RgbColor {
	uint8_t r = 0;
	uint8_t g = 0;
	uint8_t b = 0;
	uint8_t d = 0;
};

struct EffectInfo {
	uint16_t id;
	std::string name;
};

enum FileType {EFFECT};
enum FxParameter {ATTRIBUTE, WAVEFORM, SPEED, RATE, DIRECTION, 
	GROUPS, HIGH, LOW, DUTYCYCLE, WINGS, COL_D, COL_R, COL_G, COL_B};
enum WaveFormType {SIN, RAMP, INVRAMP, STEP};
enum Direction {FORWARD, REVERSE, BOUNCE_FWD, BOUNCE_BCK};
enum Attribute {DIM, COLOR_R, COLOR_G, COLOR_B};