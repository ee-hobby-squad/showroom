/*id
* name
* attribute
* form
* direction
* bpm
* rate
* phase
* high
* low
* dutycycle
* groups
* blocks
* wings
* color d
* color r
* color g
* color b
*/
#pragma once

#include <string>
#include "EngineStructures.h"


// Initialization done using:
// { 1, "Test1", DIM, RAMP, FORWARD, 90, 1, 360, 100, 0, 50, 1, 1, 0, {255, 255, 0, 0} }

struct Effect {
	EffectInfo info;

	Attribute attribute = DIM;
	WaveFormType form = SIN;
	Direction direction;

	float bpm = 120;
	float rate = 1;
	uint16_t phase = 0;

	int16_t high, low;
	uint16_t dutycycle, groups, blocks, wings;

	RgbColor color;
};


