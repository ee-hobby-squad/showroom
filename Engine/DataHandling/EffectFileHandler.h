#pragma once
#include "FileHandler.h"

#include <vector>

std::istream& operator>>(std::istream& is, Effect& f);
std::ostream& operator<<(std::ostream& os, const Effect& f);

std::istream& operator>>(std::istream& is, EffectInfo& fi);
std::ostream& operator<<(std::ostream& os, const EffectInfo& fi);



class EffectFileHandler : public FileHandler
{
public:
	EffectFileHandler();
	~EffectFileHandler();

	Effect getEffect(uint16_t effNum);
	bool setEffect(Effect effect);
	void getEffectsInfo(std::vector<EffectInfo>* allEffects);


private:

};
