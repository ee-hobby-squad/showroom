#include "FileHandler.h"



std::ostream& operator<<(std::ostream& os, const RgbColor& c) 
{
    os << std::to_string(c.r) << std::endl;
    os << std::to_string(c.g) << std::endl;
    os << std::to_string(c.b) << std::endl;
    os << std::to_string(c.d);
    return os;
}

std::istream& operator>>(std::istream& is, RgbColor& c)
{
    uint16_t inputBuf;
    is >> inputBuf;
    c.r = inputBuf;    
    is >> inputBuf;
    c.g = inputBuf;    
    is >> inputBuf;
    c.b = inputBuf;    
    is >> inputBuf;
    c.d = inputBuf;
    return is;
}


FileHandler::FileHandler()
{

}

FileHandler::~FileHandler()
{

}

// Function that returns the name of the file, dependent on the tyoe and ID
std::string FileHandler::getFileName(FileType type, uint16_t num)
{
    std::string name;
    name.append("userdata/");


    switch (type)
    {
    case EFFECT:
        name.append("effects/fx");
        name.append(std::to_string(num));
        name.append(".txt");
        break;
    }

    return name;
}