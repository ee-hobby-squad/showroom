#include "EffectFileHandler.h"

#include <algorithm>

EffectFileHandler::EffectFileHandler()
{

}
EffectFileHandler::~EffectFileHandler() 
{

}


// Function that reads an effect to a file
Effect EffectFileHandler::getEffect(uint16_t effNum)
{
    Effect f;

    std::ifstream input(getFileName(EFFECT, effNum));
    // wxLogDebug("<StorageHandle> Reading from: %s", getFileName(EFFECT, effNum));

    input >> f;
    return f;
}


// Function that stores an effect to a file
bool EffectFileHandler::setEffect(Effect effect)
{
    std::string fileName = getFileName(EFFECT, effect.info.id);
    std::ofstream output(fileName);
    wxLogDebug("<StorageHandle> Writing to: %s", fileName);

    if (output.is_open()) {
        output << effect;
    }
    else {
        return false;
    }

    output.close();
    return true;
}


void EffectFileHandler::getEffectsInfo(std::vector<EffectInfo>* allEffects)
{
    allEffects->clear();
    EffectInfo i;
    for (auto& it : std::filesystem::directory_iterator("userdata/effects")) {
        std::ifstream input(it.path());
        input >> i;
        allEffects->push_back(i);
    }
    std::sort(allEffects->begin(),
              allEffects->end(),
              [](EffectInfo &ei1, EffectInfo &ei2)
              {
                  return ei1.id < ei2.id;
              });
}


std::istream& operator>>(std::istream& is, Effect& f)
{
    unsigned int intBuf = 0;
    float fltBuf = 0.0;

    // Get all fx information from the file
    is >> f.info;

    is >> intBuf;
    f.attribute = static_cast<Attribute>(intBuf);

    is >> intBuf;           // Read form
    f.form = static_cast<WaveFormType>(intBuf);

    is >> intBuf;           // Read direction
    f.direction = static_cast<Direction>(intBuf);

    is >> fltBuf;           // Read bpm
    f.bpm = fltBuf;
    is >> fltBuf;           // Read rate
    f.rate = fltBuf;
    is >> fltBuf;           // Read phase
    f.phase = fltBuf;

    is >> f.high;           // Read high
    is >> f.low;            // Read low
    is >> intBuf;           // Read duty
    f.dutycycle = intBuf;
    is >> f.groups;
    is >> f.blocks;
    is >> f.wings;
    is >> f.color;

    return is;
}

std::ostream& operator<<(std::ostream& os, const Effect& f)
{
    os << f.info << std::endl;
    os << f.attribute << std::endl;
    os << f.form << std::endl;
    os << f.direction << std::endl;
    os << f.bpm << std::endl;
    os << f.rate << std::endl;
    os << f.phase << std::endl;
    os << f.high << std::endl;
    os << f.low << std::endl;
    os << f.dutycycle << std::endl;
    os << f.groups << std::endl;
    os << f.blocks << std::endl;
    os << f.wings << std::endl;
    os << f.color;

    return os;
}


std::istream& operator>>(std::istream& is, EffectInfo& fi) {
    is >> fi.id;                // Read ID
    is.ignore();                // Ignore NL character
    std::getline(is, fi.name);  // Read name      
    return is;
}

std::ostream& operator<<(std::ostream& os, const EffectInfo& fi) {
    os << fi.id << std::endl;
    os << fi.name;
    return os;
}
