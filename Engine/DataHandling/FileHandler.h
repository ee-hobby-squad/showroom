#pragma once

#include <fstream>
#include <iostream>
#include <filesystem>
#include <string>

#include "wx/log.h"

#include "../Utils/EngineStructures.h"
#include "../Utils/Effect.h"


std::ostream& operator<<(std::ostream& os, const RgbColor& c);
std::istream& operator>>(std::istream& is, RgbColor& c);


class FileHandler
{
public:
	FileHandler();
	~FileHandler();

protected:
	std::string getFileName(FileType type, uint16_t num);

private:

};

