
#include "FxRunner.h"
#include "Config/Configuration.h"


// Constructor
FxRunner::FxRunner(std::vector<uint8_t>* led, Effect* FX) {
	leds = led;
	curFx = FX;

	mConfig = ConfigurationManager::loadConfig();

	colBuf = curFx->color;

	switch (curFx->attribute)
	{
	case DIM:
		rgbSetter = rgbSetter_D;
		break;
	case COLOR_R:
		rgbSetter = rgbSetter_R;
		break;
	case COLOR_G:
		rgbSetter = rgbSetter_G;
		break;
	case COLOR_B:
		rgbSetter = rgbSetter_B;
		break;
	}

	if (curFx->direction == BOUNCE_FWD || curFx->direction == BOUNCE_BCK) {
		bounce = true;
	}

	phase.setGroups(curFx->groups);
	startPh = curFx->phase;
	generateWave();
	calcSpeedIncr();
	calcWings();
}


// Function updates rgb values of a new frame
void FxRunner::effectUpdate()
{
	uint16_t phaseBuf = 0;
	phase.setStartPhase(startPh);		// Set the start phase to the correct value,

	startPh += speedIncr;			// This is done for the next frame

	//TODO: bounce


	if (startPh > 360) {
		if (bounce) {
			speedIncr = -speedIncr;
			startPh = 720 - startPh;
		}
		else
			startPh -= 360;
	}
	else if (startPh < 0) {
		if (bounce) {
			speedIncr = -speedIncr;
			startPh = -startPh;
		}
		else
			startPh += 360;
	}

	//Apply the effect to all leds
	unsigned ledCnt = 0;
	int indx = 0;

	if (curFx->wings == 0 || curFx->wings == 1) {
		while (ledCnt < mConfig.numberOfLeds) {
			phaseBuf = phase.setForNextLed();
			valueBuf = 255 * wave[phaseBuf];
			rgbSetter->setRgbValue(&colBuf, valueBuf);
			setColorSingle(ledCnt);
			ledCnt++;
		}
	}
	else {
		while (ledCnt < wingGroupSize) {
			phaseBuf = phase.setForNextLed();
			valueBuf = 255 * wave[phaseBuf];
			rgbSetter->setRgbValue(&colBuf, valueBuf);
			for (int w = 0; w < curFx->wings; w++) {
				if (w % 2 == 0) 
					indx = wingGroupSize * w + ledCnt;
				else
					indx = wingGroupSize * (w+1) - ledCnt - 1;

				setColorSingle(indx);
			}
			ledCnt++;
		}
	}
}


// Function that generates a wave value array
void FxRunner::generateWave()
{
	//reset the counter
	waveVal = 0;
	
	//TODO: Make this work with floats!
	highF = (float)curFx->high * 0.01;
	lowF = (float)curFx->low * 0.01;
	hiloDif = curFx->high - curFx->low;
	hiloAvr = (curFx->high + curFx->low) / 2;

	switch (curFx->form)
	{
	case SIN:
		for (auto& w : wave) {
			w = (hiloDif * 0.5 * sin(waveVal * 2 * 3.1415 / WAVE_WIDTH) + 0.5 + hiloAvr) / 100;

			if (w < 0) {
				w = 0;
			}
			else if (w > 1) {
				w = 1;
			}
			waveVal++;
		}
		break;

	case RAMP:
		for (auto& w : wave) {
			w = (hiloDif * waveVal / (WAVE_WIDTH - 1) + hiloAvr - (hiloDif / 2)) / 100;

			if (w < 0) {
				w = 0;
			}
			else if (w > 1) {
				w = 1;
			}
			waveVal++;
		}
		break;

	case INVRAMP:
		for (auto& w : wave) {
			w = (hiloDif * (WAVE_WIDTH - waveVal) / (WAVE_WIDTH - 1) + hiloAvr - (hiloDif / 2)) / 100;

			if (w < 0) {
				w = 0;
			}
			else if (w > 1) {
				w = 1;
			}
			waveVal++;
		}

		break;

	case STEP:
		float hiF = (float)curFx->high * 0.01;
		float loF = (float)curFx->low * 0.01;

		for (auto& w : wave) {
			if (waveVal < curFx->dutycycle * 0.01 * (WAVE_WIDTH - 1)) {
				w = hiF;
			}
			else {
				w = loF;
			}
			waveVal++;
		}

		break;
	}
}


void FxRunner::calcWings() {
	if (curFx->wings > 0)
		wingGroupSize = mConfig.numberOfLeds / curFx->wings;
	else
		wingGroupSize = mConfig.numberOfLeds;
}

//Function that sets the color of a single LED
void FxRunner::setColorSingle(uint16_t ledNr)
{
	uint16_t ledIt = 3 * ledNr;
	(*leds)[ledIt] = colBuf.r * colBuf.d / 255;
	(*leds)[ledIt + 1] = colBuf.g * colBuf.d / 255;
	(*leds)[ledIt + 2] = colBuf.b * colBuf.d / 255;
}


// Function that calculates the increment in the wave array per frame
void FxRunner::calcSpeedIncr()
{
	speedIncr = (int)(curFx->bpm * curFx->rate * WAVE_WIDTH) * 0.000277;			// bpm*rate*wavewidth/60 seconds/30 frames * 0.5?
	if (curFx->direction == REVERSE || curFx->direction == BOUNCE_BCK)
		speedIncr = -speedIncr;
}


//void FxRunner::onConfigChange()
//{
//	mConfig = ConfigurationManager::loadConfig();
//	leds->resize(mConfig.numberOfLeds * 3);
//}
