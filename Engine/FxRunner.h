#pragma once

#include <cstdint>
#include <array>
#include <vector>
#include <string>
#include <cmath>

#include "DataHandling/EffectFileHandler.h"
#include "FxPhase.h"
#include "Utils/EngineStructures.h"
#include "Config/Configuration.h"
#include "Utils/MutexedData.h"
#include "Utils/Effect.h"


#define FRAME_RATE 30
#define INTERVAL 1000000/FRAME_RATE

#define WAVE_WIDTH 1024



//***********************************************************
class RgbSetter {
public:
	virtual void setRgbValue(RgbColor* col, uint8_t val) = 0;
};

class RgbSetter_D : public RgbSetter {
public:
	void setRgbValue(RgbColor* col, uint8_t val) {
		col->d = val;
	}
};

class RgbSetter_R : public RgbSetter {
public:
	void setRgbValue(RgbColor* col, uint8_t val) {
		col->r = val;
	}
};

class RgbSetter_G : public RgbSetter {
public:
	void setRgbValue(RgbColor* col, uint8_t val) {
		col->g = val;
	}
};

class RgbSetter_B : public RgbSetter {
public:
	void setRgbValue(RgbColor* col, uint8_t val) {
		col->b = val;
	}
};
// ***********************************************************



class FxRunner
{
public:
	FxRunner(std::vector<uint8_t>* led, Effect* FX);
	
	void effectUpdate();
	void setEffectCol(RgbColor col);
	void onConfigChange();


protected:
	void setColorSingle(uint16_t ledNr);
	void calcSpeedIncr();
	void generateWave();
	void calcWings();

	ShowRoomConfig mConfig;

	std::vector<uint8_t>* leds = nullptr;
	FxPhase phase;

	//fx foreground parameters (stored inside effect structure)
	Effect* curFx = nullptr;
	RgbColor colBuf;

	//fx background values
	int16_t startPh = 0;
	uint16_t speedIncr = 0;
	uint8_t dim = 0;
	float highF;
	float lowF;
	int16_t hiloDif;
	int16_t hiloAvr;
	uint8_t valueBuf;
	uint16_t wingGroupSize;
	bool bounce = false;

	RgbSetter* rgbSetter;
	RgbSetter_D* rgbSetter_D = new RgbSetter_D();
	RgbSetter_R* rgbSetter_R = new RgbSetter_R();
	RgbSetter_G* rgbSetter_G = new RgbSetter_G();
	RgbSetter_B* rgbSetter_B = new RgbSetter_B();

	std::array <float, WAVE_WIDTH> wave = {};
	float waveVal = 0;
};
