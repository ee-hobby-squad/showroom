#include "Core.hpp"

namespace
{
    bool isInitialized = false;

    MutexedData* mRGBData;
	MainEngine* mMainEngine;
    DataGate* mDataGate;
}


namespace SR
{
    void init(std::optional<std::string> luaScriptPath)
    {
        if (isInitialized) return;
        isInitialized = true;

        auto config = ConfigurationManager::loadConfig();
        mRGBData    = new MutexedData(config.numberOfLeds * 3);
        mMainEngine = new MainEngine(*mRGBData, luaScriptPath);
        mDataGate   = new DataGate(mRGBData);
    }

    MainEngine *getMainEngine() { return mMainEngine; }
    DataGate *getDataGate() { return mDataGate; }
}
