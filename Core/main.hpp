#ifndef MAIN_HPP
#define MAIN_HPP

#include "wx/wx.h"
#include "wx/cmdline.h"
#include "../Interface/ShowRoom.h"

extern const wxCmdLineEntryDesc parserDesc[] = {
    {
        wxCMD_LINE_SWITCH,
        "h",
        "help",
        "displays help",
        wxCMD_LINE_VAL_NONE,
        wxCMD_LINE_OPTION_HELP
    },
    {
        wxCMD_LINE_SWITCH,
        "c",
        "headless",
        "run without gui",
        wxCMD_LINE_VAL_NONE,
        wxCMD_LINE_PARAM_OPTIONAL
    },
    {
        wxCMD_LINE_PARAM,
        nullptr,
        nullptr,
        "script",
        wxCMD_LINE_VAL_STRING,
        wxCMD_LINE_PARAM_OPTIONAL
    },
    {
        wxCMD_LINE_NONE,
        nullptr,
        nullptr,
        nullptr,
        wxCMD_LINE_VAL_NONE,
        wxCMD_LINE_PARAM_OPTIONAL
    }
};

ShowRoom &wxGetApp();
wxAppConsole *wxCreateApp();
wxAppInitializer wxTheAppInitializer((wxAppInitializerFunction) wxCreateApp);

#endif
