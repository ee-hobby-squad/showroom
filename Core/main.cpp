#include "main.hpp"
#include "Core.hpp"

#if defined(__VISUALC__)
int wmain(int argc, wchar_t **argv)
#else
int main(int argc, char **argv)
#endif
{
    bool headless = false;
    std::optional<std::string> luaScriptPath;

    wxMessageOutput::Set(new wxMessageOutputBest);
    wxCmdLineParser parser(parserDesc, argc, argv);

    int res = parser.Parse();
    if (res == -1) return 0;
    if (res == 0)
    {
        headless = parser.Found(wxT("c"));

        wxString value;
        if (parser.GetParamCount() > 0)
        {
            value = parser.GetParam(0);
            luaScriptPath = {value.ToStdString()};
        }
        else
        {
            luaScriptPath = std::nullopt;
        }
    }

    SR::init(luaScriptPath);

    if (!headless)
    {
        return wxEntry(argc, argv);
    }

    while (true)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }

    // TODO: Headless
    return 0;
}

ShowRoom &wxGetApp()
{
    return *static_cast<ShowRoom*>(wxApp::GetInstance());
}

wxAppConsole *wxCreateApp()
{
    wxAppConsole::CheckBuildOptions(WX_BUILD_OPTIONS_SIGNATURE, "showRoom");
    return new ShowRoom;
}
