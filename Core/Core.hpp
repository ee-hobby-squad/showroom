#ifndef SHOWROOM_CORE_HPP
#define SHOWROOM_CORE_HPP

#include <optional>
#include <string>

#include "../Engine/MainEngine.h"
#include "../Engine/Output/DataGate.h"

namespace SR
{
    void init(std::optional<std::string> luaScriptPath);
    MainEngine *getMainEngine();
    DataGate *getDataGate();
}

#endif
