cmake_minimum_required(VERSION "3.16")

project("showRoom")

file(GLOB_RECURSE SOURCES RELATIVE ${CMAKE_SOURCE_DIR} "Engine/*.cpp" "Interface/*.cpp" "Core/*.cpp")
file(GLOB_RECURSE TEST_SOURCES RELATIVE ${CMAKE_SOURCE_DIR} "Engine/*.cpp"  "Interface/*.cpp" "tests/*.cpp" "Core/*.cpp")
list(REMOVE_ITEM TEST_SOURCES "Core/main.cpp")

set(wxWidgets_USE_LIBS)

find_package(wxWidgets REQUIRED)
find_package(GTest)
find_package(Lua REQUIRED)

include(${wxWidgets_USE_FILE})

include_directories(${wxWidgets_INCLUDE_DIRS})
include_directories(${LUA_INCLUDE_DIR})

SET(CMAKE_CXX_FLAGS  "-std=c++17 \
                      -Wall \
                      -Wextra")

add_executable("${PROJECT_NAME}" "${SOURCES}")
IF( GTest_FOUND )
    add_executable("test" "${TEST_SOURCES}")
ENDIF()

IF( GTest_FOUND )
    target_compile_options("test" PRIVATE "-DNO_MAIN" "-fprofile-arcs" "-ftest-coverage" "-fPIC")
    target_link_libraries("test" "-lgmock" "-lgcov" "--coverage")
ENDIF()

set(WindowsLinkerFlags "")
IF( WIN32 )
    list(APPEND WindowsLinkerFlags "-lWs2_32")
ENDIF()

target_link_libraries("${PROJECT_NAME}" ${wxWidgets_LIBRARIES} ${LUA_LIBRARIES} ${WindowsLinkerFlags})
IF( GTest_FOUND )
    target_link_libraries("test" ${wxWidgets_LIBRARIES} ${LUA_LIBRARIES} GTest::GTest GTest::Main ${WindowsLinkerFlags})
ENDIF()
