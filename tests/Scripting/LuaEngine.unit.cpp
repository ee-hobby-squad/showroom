#include <gtest/gtest.h>

#include <filesystem>
#include <fstream>

#include "../../Engine/Scripting/LuaEngine.h"

int testCFunction(lua_State *state)
{
    lua_getglobal(state, "a");
    int a = lua_tointeger(state, lua_gettop(state));

    lua_getglobal(state, "b");
    int b = lua_tointeger(state, lua_gettop(state));

    int c = a + b;
    lua_pushinteger(state, c);
    lua_setglobal(state, "c");

    return 1;
}

namespace
{
    TEST(LuaEngine, InitAndDestroy)
    {
        LuaEngine e;
        e.initLua();

        EXPECT_TRUE(e.isInitialized());

        e.destroyLua();

        EXPECT_FALSE(e.isInitialized());
    }

    TEST(LuaEngine, RunStringAndGetGlobal)
    {
        LuaEngine e;
        e.initLua();

        e.runString("a = 1");
        e.runString("b = 2");
        e.runString("c = 3");
        e.runString("d = a + b + c");

        EXPECT_EQ(e.getGlobalInt("a"), 1);
        EXPECT_EQ(e.getGlobalInt("b"), 2);
        EXPECT_EQ(e.getGlobalInt("c"), 3);
        EXPECT_EQ(e.getGlobalInt("d"), 6);

        e.destroyLua();
    }

    TEST(LuaEngine, RunFileAndGetGlobal)
    {
        // Create the file to be tested
        std::filesystem::path filePath{"lua-engine-unit-test.lua"};
        std::ofstream fileStream{filePath};

        // Insert some lua code
        fileStream << "a = 1\nb = 2\nc = 3\nd = (a + b) * c" << std::endl;

        // Close the file
        fileStream.close();

        // Initialize the engine
        LuaEngine e;
        e.initLua();

        // Run the code in the file
        e.runFile(filePath.string());

        // Check the global values if they match the expectations
        EXPECT_EQ(e.getGlobalInt("a"), 1);
        EXPECT_EQ(e.getGlobalInt("b"), 2);
        EXPECT_EQ(e.getGlobalInt("c"), 3);
        EXPECT_EQ(e.getGlobalInt("d"), 9);

        // Cleanup the lua engine
        e.destroyLua();

        // Remove the created file
        std::filesystem::remove(filePath);
    }

    TEST(LuaEngine, ExposeFunctionGetGlobal)
    {
        LuaEngine e;
        e.initLua();

        e.exposeFunction(testCFunction, "addGlobals", nullptr);

        e.runString("a = 3");
        e.runString("b = 5");

        EXPECT_EQ(e.getGlobalInt("a"), 3);
        EXPECT_EQ(e.getGlobalInt("b"), 5);

        e.runString("addGlobals()");

        EXPECT_EQ(e.getGlobalInt("c"), 8);

        e.destroyLua();
    }

    TEST(LuaEngine, GetStateReturnsCorrectState)
    {
        LuaEngine e;
        e.initLua();

        lua_State *state = e.getState();

        lua_pushinteger(state, 9);
        lua_setglobal(state, "a");

        EXPECT_EQ(e.getGlobalInt("a"), 9);

        e.destroyLua();
    }
}
