#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <thread>
#include <chrono>

#include "../../../Engine/Communication/UDP/BufferedUDPServer.h"
#include "../../../Engine/Communication/UDP/UDPServer.h"
#include "../../../Engine/Communication/UDP/UDPCommunicationProvider.h"

class EmptyUDPServer : public UDPServer
{
public:
    bool bind([[maybe_unused]] const UDPSocketAddress &address) override
    {
        return true;
    };
    void close() override {};
    std::pair<UDPSocketAddress, std::vector<uint8_t>> read() const override
    {
        return {};
    };
    void send([[maybe_unused]] const UDPSocketAddress &,
              [[maybe_unused]] const std::vector<uint8_t> &) const override
    {
    };
};

class MockBufferedUDPServer : public BufferedUDPServer
{
    private:
        bool isRunning = true;
        void readerFunc() override
        {
            while (isRunning)
            {
                std::this_thread::sleep_for(std::chrono::milliseconds(5));
            }
        }
    public:
        MockBufferedUDPServer(EmptyUDPServer &s) : BufferedUDPServer(s) {}
        ~MockBufferedUDPServer() {isRunning = false;}
        MOCK_METHOD(void, sendTo, (const UDPSocketAddress &,
                                   const std::vector<uint8_t> &), (override));

        MOCK_METHOD(std::vector<uint8_t>, recvFrom, (const UDPSocketAddress &), (override));
};

namespace
{
    TEST(UDPCommunicationProvider, CanSend)
    {
        EmptyUDPServer s;
        MockBufferedUDPServer mockBufferedUDPServer{s};
        EXPECT_CALL(mockBufferedUDPServer, sendTo(
                    (UDPSocketAddress){"localhost", 1000},
                    (std::vector<uint8_t>){0x00, 0x01, 0x02}));

        UDPCommunicationProvider provider(mockBufferedUDPServer,
                                          (UDPSocketAddress){"localhost", 1000});

        provider.send({0x00, 0x01, 0x02});
    }

    TEST(UDPCommunicationProvider, CanReceive)
    {
        EmptyUDPServer s;
        MockBufferedUDPServer mockBufferedUDPServer{s};
        EXPECT_CALL(mockBufferedUDPServer,
                    recvFrom((UDPSocketAddress){"localhost", 1000}));

        UDPCommunicationProvider provider(mockBufferedUDPServer,
                                          (UDPSocketAddress){"localhost", 1000});

        provider.recv();
    }
}
