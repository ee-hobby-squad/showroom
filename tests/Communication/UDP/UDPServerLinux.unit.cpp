#if !defined(WIN32) && !defined(_WIN64)
#include <gtest/gtest.h>

// Linux headers
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>

// Standard library headers
#include <cstring>
#include <thread>
#include <chrono>
#include <future>

#include "../../../Engine/Communication/UDP/UDPServerLinux.h"

namespace
{
    TEST(LinuxUDPServer, CanBindToNormalPort)
    {
        UDPServerLinux server;
        ASSERT_TRUE(server.bind({"localhost", 6603}));
        ASSERT_NE(server.getFileDescriptor(), -1);
    }

    void sendDataAfterTime(const std::vector<uint8_t> &data)
    {
        int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
        if (sockfd == -1) return;

        sockaddr_in serverAddress;
        serverAddress.sin_family = AF_INET;
        serverAddress.sin_port = htons(6603);
        serverAddress.sin_addr.s_addr = INADDR_ANY;


        sendto(sockfd, data.data(), data.size(), MSG_CONFIRM, 
               (sockaddr*)&serverAddress, sizeof(serverAddress));
    }

    TEST(LinuxUDPServer, CanReceiveData)
    {
        UDPServerLinux server;
        ASSERT_TRUE(server.bind({"localhost", 6603}));


        std::string data = "Foo Bar Baz";
        std::vector<uint8_t> dataVec{data.begin(), data.end()};

        std::thread t{sendDataAfterTime, dataVec};

        auto readData = server.read();

        EXPECT_EQ(dataVec, readData.second);
        EXPECT_EQ(std::string{"127.0.0.1"}, readData.first.host);

        t.join();
    }

    TEST(LinuxUDPServer, CannotReceiveIfNotBound)
    {
        UDPServerLinux server;

        std::string data = "Foo Bar Baz";
        std::vector<uint8_t> dataVec{data.begin(), data.end()};

        std::thread t{sendDataAfterTime, dataVec};

        EXPECT_EQ(std::vector<uint8_t>{}, server.read().second);

        t.join();
    }

    void readMessage(int socket, std::promise<std::vector<uint8_t>> &p)
    {
        sockaddr_in adr;
        unsigned int adrlen = sizeof(adr);

        uint8_t buffer[64 * 1024];
        int len = recvfrom(socket, buffer, 64*1024, MSG_WAITALL,
                           (sockaddr*)&adr, &adrlen);

        p.set_value({buffer, buffer+len});

        return;
    }

    TEST(LinuxUDPServer, CanSendData)
    {
        UDPSocketAddress to{"localhost", 6604};

        addrinfo hints, *res, *itr;

        memset(&hints, 0, sizeof(hints));
        hints.ai_family = AF_INET;
        hints.ai_socktype = SOCK_DGRAM;

        getaddrinfo("localhost", "6604", &hints, &res);

        int sfd;
        for (itr = res; itr != NULL; itr = itr->ai_next) {
            sfd = socket(itr->ai_family, itr->ai_socktype,
                         itr->ai_protocol);
            if (sfd == -1)
                continue;

            if (bind(sfd, itr->ai_addr, itr->ai_addrlen) == 0)
                break;                  /* Success */

            close(sfd);
        }

        if (itr == NULL) {
            SCOPED_TRACE("Could not bind to port 6604");
            ASSERT_TRUE(false);
        }

        std::promise<std::vector<uint8_t>> prom;

        std::thread t{readMessage, sfd, std::ref(prom)};

        UDPServerLinux server;
        ASSERT_TRUE(server.bind({"localhost", 6603}));

        std::string data = "Foo Bar Baz";
        std::vector<uint8_t> dataVec{data.begin(), data.end()};

        server.send(to, dataVec);

        auto val = prom.get_future().get();

        EXPECT_EQ(val, dataVec);
        t.join();
    }

    TEST(LinuxUDPAddr, HostnameAndIPEqual)
    {
        UDPSocketAddress s1{"localhost", 3000};
        UDPSocketAddress s2{"127.0.0.1", 3000};
        EXPECT_EQ(s1, s2);
    }

    TEST(LinuxUDPAddr, DifferentPortNotEqual)
    {
        UDPSocketAddress s1{"127.0.0.1", 3000};
        UDPSocketAddress s2{"127.0.0.1", 3001};
        EXPECT_FALSE(s1 == s2);
    }

    TEST(LinuxUDPAddr, DifferentPortLessThan)
    {
        UDPSocketAddress s1{"127.0.0.1", 3000};
        UDPSocketAddress s2{"127.0.0.1", 3001};
        EXPECT_LT(s1,s2);
    }
}
#endif