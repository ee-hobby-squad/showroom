#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <thread>
#include <chrono>

#include "../../../Engine/Communication/UDP/UDPServer.h"
#include "../../../Engine/Communication/UDP/BufferedUDPServer.h"

class MockUDPServer : public UDPServer
{
    public:
        MOCK_METHOD(bool, bind, (const UDPSocketAddress &address), (override));

        MOCK_METHOD(void, close, (), (override));

        MOCK_METHOD((std::pair<UDPSocketAddress, std::vector<uint8_t>>),
                    read, (), (const, override));

        MOCK_METHOD(void, send, (const UDPSocketAddress &address,
                                 const std::vector<uint8_t> &data),
                    (const, override));
};

namespace
{
    TEST(BufferedUDPServer, CanSendToAddress)
    {
        using ::testing::Return;

        MockUDPServer mockServer;
        EXPECT_CALL(mockServer, read())
            .WillRepeatedly(Return(std::pair<UDPSocketAddress, std::vector<uint8_t>>{
                UDPSocketAddress{"localhost", 1000},
                std::vector<uint8_t>{}
            }));

        BufferedUDPServer udpServer(mockServer);
        udpServer.start();

        EXPECT_CALL(mockServer, send(UDPSocketAddress{"localhost", 1000}, std::vector<uint8_t>{'a','b'}));
        udpServer.sendTo({"127.0.0.1", 1000}, {'a', 'b'});
    }

    TEST(BufferedUDPServer, CorrectFIFOBuffer)
    {
        using ::testing::Return;

        MockUDPServer mockServer;
        EXPECT_CALL(mockServer, read())
            .WillOnce(Return(std::pair<UDPSocketAddress, std::vector<uint8_t>>{
                UDPSocketAddress{"localhost", 1000},
                std::vector<uint8_t>{'a'}
            }))
            .WillOnce(Return(std::pair<UDPSocketAddress, std::vector<uint8_t>>{
                UDPSocketAddress{"localhost", 1000},
                std::vector<uint8_t>{'b'}
            }))
            .WillOnce(Return(std::pair<UDPSocketAddress, std::vector<uint8_t>>{
                UDPSocketAddress{"localhost", 1001},
                std::vector<uint8_t>{'a'}
            }))
            .WillOnce(Return(std::pair<UDPSocketAddress, std::vector<uint8_t>>{
                UDPSocketAddress{"localhost", 1000},
                std::vector<uint8_t>{'c'}
            }))
            .WillOnce(Return(std::pair<UDPSocketAddress, std::vector<uint8_t>>{
                UDPSocketAddress{"localhost", 1001},
                std::vector<uint8_t>{'b'}
            }))
            .WillOnce(Return(std::pair<UDPSocketAddress, std::vector<uint8_t>>{
                UDPSocketAddress{"localhost", 1001},
                std::vector<uint8_t>{'c'}
            }))
            .WillRepeatedly(Return(std::pair<UDPSocketAddress, std::vector<uint8_t>>{
                UDPSocketAddress{"localhost", 1000},
                std::vector<uint8_t>{}
            }));

        BufferedUDPServer udpServer(mockServer);
        udpServer.start();

        std::this_thread::sleep_for(std::chrono::milliseconds(30));
        EXPECT_EQ(std::vector<uint8_t>{'a'}, udpServer.recvFrom({"localhost",1001}));
        EXPECT_EQ(std::vector<uint8_t>{'b'}, udpServer.recvFrom({"localhost",1001}));
        EXPECT_EQ(std::vector<uint8_t>{'c'}, udpServer.recvFrom({"localhost",1001}));
        EXPECT_EQ(std::vector<uint8_t>{'a'}, udpServer.recvFrom({"localhost",1000}));
        EXPECT_EQ(std::vector<uint8_t>{'b'}, udpServer.recvFrom({"localhost",1000}));
        EXPECT_EQ(std::vector<uint8_t>{'c'}, udpServer.recvFrom({"localhost",1000}));
    }
}
