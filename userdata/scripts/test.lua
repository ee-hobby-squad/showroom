math.randomseed(os.time())
function randInt(low, upp)
    local low = math.floor(low)
    local upp = math.floor(upp)
    return math.floor(math.random() * (upp - low + 1) + low)
end

print("\n\n")
print("------- random.lua -------")

while true do
    fxs = getEffects()
    newId = 5
    newEffect = {}
    newEffect.info = {
        ["id"] = newId,
        ["name"] = "random fx #" .. newId
    }
    newEffect.attribute = randInt(0, 3)
    newEffect.form = randInt(0, 3)
    newEffect.direction = randInt(0, 3)
    newEffect.bpm = randInt(5, 30)
    newEffect.rate = randInt(1, 4)
    newEffect.phase = randInt(0, 360)
    newEffect.high = randInt(0, 100)
    newEffect.low = randInt(-100, 0)
    newEffect.dutycycle = randInt(0, 100)
    newEffect.groups = randInt(1, 50)
    newEffect.blocks = randInt(0, 50)
    newEffect.wings = randInt(0, 10)
    newEffect.color = {
        ["r"] = randInt(0, 255),
        ["g"] = randInt(0, 255),
        ["b"] = randInt(0, 255),
        ["d"] = randInt(0, 255)
    }

    print("Starting this new effect");
    startEffect(newEffect)
    sleep(2000)
end
print("\n\n")
