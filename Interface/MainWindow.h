#pragma once

#include "wx/wx.h"
#include <optional>
#include <string>

#include "wxIDs.h"
#include "Icons.h"
#include "EffectWindow.h"
#include "ConfigWindow.h"
#include "../Engine/MainEngine.h"
#include "../Engine/Output/DataGate.h"


class MainWindow : public wxFrame
{
public:
	MainWindow();
	~MainWindow();

	wxMenuBar* menubar = nullptr;
	wxMenu* fileMenu = nullptr;
	wxMenu* toolMenu = nullptr;

	wxToolBar* toolbar = nullptr;

	wxButton* m_btn1 = nullptr;
	wxButton* m_btn2 = nullptr;
	wxButton* m_btn3 = nullptr;

	wxListBox* fxList = nullptr;

	wxSlider* sld1 = nullptr;
	wxSlider* sld2 = nullptr;
	wxSlider* sld3 = nullptr;
	wxSlider* sld4 = nullptr;

	void OnButtonClicked(wxCommandEvent& evt);
	void sliderChange(wxCommandEvent& evt);
	void onMenuClick(wxCommandEvent& evt);
	void onToolClick(wxCommandEvent& evt);
	void onListBoxSelect(wxCommandEvent& evt);
	void OnQuit(wxCommandEvent& evt);

	wxDECLARE_EVENT_TABLE();

private:
	EffectWindow* effectWindow = nullptr;
	ConfigWindow* configWindow = nullptr;

	std::vector<EffectInfo>* effectList = nullptr;

	void initToolbar();

	// Enum of tools in toolbar
	enum {
		Tool_add,
		Tool_edit,
		Tool_delete,
		//Tool_next,
		//Tool_previous
	};
};
