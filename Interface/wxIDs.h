/*
 * In this file all widget ids from the frontend are stored (buttons, sliders etc)
 * 
 */

//Toolbar
#define wxID_TOOL_ADD 10201
#define wxID_TOOL_EDIT 10202
#define wxID_TOOL_DELETE 10203
#define wxID_TOOL_NEXT 10211
#define wxID_TOOL_PREVIOUS 10212

// Main window mode select
#define wxID_MODE_STATCOL 10001
#define wxID_MODE_RAINBOW 10002
#define wxID_MODE_EFFECT 10003

// Main window static color control
#define	wxID_SLD_STATCOL_DIM 10101
#define wxID_SLD_STATCOL_RED 10102
#define wxID_SLD_STATCOL_GREEN 10103
#define wxID_SLD_STATCOL_BLUE 10104

// Effect stuff
#define wxID_EFFECT_LIST 11101

// Texxtctl
#define wxID_FX_STATCOL_DIM 20101 
#define wxID_FX_STATCOL_RED 20102 
#define wxID_FX_STATCOL_GREEN 20103
#define wxID_FX_STATCOL_BLUE 20104 
#define wxID_FX_SPEED 20105 
#define wxID_FX_RATE 20106 
#define wxID_FX_GROUPS 20107 
#define wxID_FX_HIGH 20108
#define wxID_FX_LOW 20109
#define wxID_FX_DUTY 20110
#define wxID_FX_WINGS 20111

// Choice
#define wxID_FX_ATTR 20201
#define wxID_FX_FORM 20202
#define wxID_FX_DIR 20203