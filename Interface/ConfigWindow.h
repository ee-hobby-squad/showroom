#pragma once

#include "wx/wx.h"
#include <wx/spinctrl.h>
#include "../Engine/MainEngine.h"
#include "../Engine/Output/DataGate.h"
#include "../Engine/Config/Configuration.h"

class ConfigWindow : public wxFrame
{
public:
	ConfigWindow(MainEngine* engine, DataGate *gate);
	~ConfigWindow();

    ShowRoomConfig mConfig;

	wxDECLARE_EVENT_TABLE();

    /** Start of widgets on this window */
    wxStaticText mCommunicationProviderTooltip;
    wxChoice mCommunicationProviderDropDown;

    wxStaticText mUDPServerIPTooltip;
    wxTextCtrl mUDPServerIPInput;

    wxStaticText mUDPServerPortTooltip;
    wxSpinCtrl mUDPServerPortInput;

    wxStaticText mUDPClientIPTooltip;
    wxTextCtrl mUDPClientIPInput;

    wxStaticText mUDPClientPortTooltip;
    wxSpinCtrl mUDPClientPortInput;

    wxStaticText mSerialPortTooltip;
    wxTextCtrl mSerialPortInput;

    wxStaticText mSerialBaudRateTooltip;
    wxSpinCtrl mSerialBaudRateInput;

    wxStaticText mNumberOfLedsTooltip;
    wxSpinCtrl mNumberOfLedsInput;
    wxButton mSaveButton;
    /** End   of widgets on this window */

    void onApplyClicked(wxCommandEvent& evt);

private:
	MainEngine *mMainEngine = nullptr;
	DataGate *mDataGate = nullptr;

    void createWidgets();
    void loadConfig();
};

