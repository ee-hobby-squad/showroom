#include "EffectWindow.h"

wxBEGIN_EVENT_TABLE(EffectWindow, wxFrame)
EVT_CLOSE(EffectWindow::OnCloseWindow)
EVT_CHOICE(wxID_FX_ATTR, EffectWindow::ChoiceValueChanged)
EVT_CHOICE(wxID_FX_FORM, EffectWindow::ChoiceValueChanged)
EVT_CHOICE(wxID_FX_DIR, EffectWindow::ChoiceValueChanged)
EVT_TEXT(wxID_FX_STATCOL_DIM, EffectWindow::TextValueChanged)
EVT_TEXT(wxID_FX_STATCOL_RED, EffectWindow::TextValueChanged)
EVT_TEXT(wxID_FX_STATCOL_GREEN, EffectWindow::TextValueChanged)
EVT_TEXT(wxID_FX_STATCOL_BLUE, EffectWindow::TextValueChanged)
EVT_TEXT(wxID_FX_SPEED, EffectWindow::TextValueChanged)
EVT_TEXT(wxID_FX_RATE, EffectWindow::TextValueChanged)
EVT_TEXT(wxID_FX_GROUPS, EffectWindow::TextValueChanged)
EVT_TEXT(wxID_FX_HIGH, EffectWindow::TextValueChanged)
EVT_TEXT(wxID_FX_LOW, EffectWindow::TextValueChanged)
EVT_TEXT(wxID_FX_DUTY, EffectWindow::TextValueChanged)
EVT_TEXT(wxID_FX_WINGS, EffectWindow::TextValueChanged)
wxEND_EVENT_TABLE()



EffectWindow::EffectWindow(MainEngine *mainEngine) : wxFrame(nullptr, wxID_ANY, "Effect Window", wxPoint(550, 250), wxSize(400, 400))
{
    this->mainEngine = mainEngine;
	this->fxEditor = mainEngine->newEffectEditor();

	wxString attributes[4] = { wxT("Dim"), wxT("Red"), wxT("Green"), wxT("Blue") };
	wxString waveforms[4] = { wxT("Sin"), wxT("Ramp"), wxT("Inv ramp"), wxT("Step") };
	wxString directions[4] = { wxT(">"), wxT("<"), wxT("Bounce >"), wxT("Bounce <")};

	// Attribute and waveform choice boxes:
	attrTxt = new wxStaticText(this, wxID_ANY, wxT("Attribute:"), wxPoint(10, 13), wxDefaultSize);
	formTxt = new wxStaticText(this, wxID_ANY, wxT("Waveform:"), wxPoint(190, 13), wxDefaultSize);
	attrBox = new wxChoice(this, wxID_FX_ATTR, wxPoint(70, 10), wxSize(80, 25), 4, attributes);
	attrBox->SetSelection(0);
	formBox = new wxChoice(this, wxID_FX_FORM, wxPoint(260, 10), wxSize(80, 25), 4, waveforms);
	formBox->SetSelection(0);

	// Static color values:
	dimTxt = new wxStaticText(this, wxID_ANY, wxT("Static dim:"), wxPoint(10, 293), wxDefaultSize);
	colorTxt = new wxStaticText(this, wxID_ANY, wxT("Static col:"), wxPoint(155, 293), wxDefaultSize);
	dimVal = new wxTextCtrl(this, wxID_FX_STATCOL_DIM, wxT("0"), wxPoint(70, 290), wxSize(30, 22));
	redVal = new wxTextCtrl(this, wxID_FX_STATCOL_RED, wxT("0"), wxPoint(220, 290), wxSize(30, 22));
	greenVal = new wxTextCtrl(this, wxID_FX_STATCOL_GREEN, wxT("0"), wxPoint(260, 290), wxSize(30, 22));
	blueVal = new wxTextCtrl(this, wxID_FX_STATCOL_BLUE, wxT("0"), wxPoint(300, 290), wxSize(30, 22));

	// Speed, rate and groups:
	speedTxt = new wxStaticText(this, wxID_ANY, wxT("Speed:"), wxPoint(10, 53), wxDefaultSize);
	rateTxt = new wxStaticText(this, wxID_ANY, wxT("Rate:"), wxPoint(180, 53), wxDefaultSize);
	dirTxt = new wxStaticText(this, wxID_ANY, wxT("Direction:"), wxPoint(10, 93), wxDefaultSize);
	groupsTxt = new wxStaticText(this, wxID_ANY, wxT("Groups:"), wxPoint(10, 133), wxDefaultSize);
	speedVal = new wxTextCtrl(this, wxID_FX_SPEED, wxT(""), wxPoint(70, 50), wxSize(80, 22));
	rateVal = new wxTextCtrl(this, wxID_FX_RATE, wxT(""), wxPoint(220, 50), wxSize(80, 22));
	dirBox = new wxChoice(this, wxID_FX_DIR, wxPoint(70, 90), wxSize(80, 25), 4, directions);
	dirBox->SetSelection(0);
	groupsVal = new wxTextCtrl(this, wxID_FX_GROUPS, wxT(""), wxPoint(70, 130), wxSize(80, 22));

	// High, low and dutycycle
	highTxt = new wxStaticText(this, wxID_ANY, wxT("High:"), wxPoint(10, 173), wxDefaultSize);
	lowTxt = new wxStaticText(this, wxID_ANY, wxT("Low:"), wxPoint(180, 173), wxDefaultSize);
	dutyTxt = new wxStaticText(this, wxID_ANY, wxT("Duty:"), wxPoint(10, 213), wxDefaultSize);
	highVal = new wxTextCtrl(this, wxID_FX_HIGH, wxT(""), wxPoint(70, 170), wxSize(80, 22));
	lowVal = new wxTextCtrl(this, wxID_FX_LOW, wxT(""), wxPoint(220, 170), wxSize(80, 22));
	dutyVal = new wxTextCtrl(this, wxID_FX_DUTY, wxT(""), wxPoint(70, 210), wxSize(80, 22));

	// Wings
	wingsTxt = new wxStaticText(this, wxID_ANY, wxT("Wings:"), wxPoint(10, 253), wxDefaultSize);
	wingsVal = new wxTextCtrl(this, wxID_FX_WINGS, wxT("0"), wxPoint(70, 250), wxSize(80, 22));

	// Request values from engine and update them
	curFx = fxEditor->getFxParameters();
	setUpdatedValues();

	// conclude initialization (inputs will now be forwarded to mainEngine
	windowInit = true;
}


void EffectWindow::OnCloseWindow(wxCloseEvent& evt) {
	wxLogDebug("<EffectWindow> The window will close!");
    mainEngine->removeRunner(fxEditor);
	delete fxEditor;
	evt.Skip();
}



// *************************************************************
//						Event handling
// *************************************************************
void EffectWindow::TextValueChanged(wxCommandEvent& evt) 
{
	double entVal = 0;
	if (!evt.GetString().ToDouble(&entVal)) {
		//TODO: set value back to last value when input invalid
	}

	if (windowInit) {
		switch (evt.GetId()) {
		case wxID_FX_STATCOL_DIM:
			fxEditor->setFxParameter(COL_D, entVal);
			break;		
		case wxID_FX_STATCOL_RED:
			fxEditor->setFxParameter(COL_R, entVal);
			break;		
		case wxID_FX_STATCOL_GREEN:
			fxEditor->setFxParameter(COL_G, entVal);
			break;		
		case wxID_FX_STATCOL_BLUE:
			fxEditor->setFxParameter(COL_B, entVal);
			break;
		case wxID_FX_SPEED:
			fxEditor->setFxParameter(SPEED, entVal);
			break;
		case wxID_FX_RATE:
			fxEditor->setFxParameter(RATE, entVal);
			break;
		case wxID_FX_GROUPS:
			fxEditor->setFxParameter(GROUPS, entVal);
			break;
		case wxID_FX_HIGH:
			fxEditor->setFxParameter(HIGH, entVal);
			break;
		case wxID_FX_LOW:
			fxEditor->setFxParameter(LOW, entVal);
			break;
		case wxID_FX_DUTY:
			fxEditor->setFxParameter(DUTYCYCLE, entVal);
			break;			
		case wxID_FX_WINGS:
			fxEditor->setFxParameter(WINGS, entVal);
			break;		

		}
	}
	//setUpdatedValues();
	evt.Skip();
}


void EffectWindow::ChoiceValueChanged(wxCommandEvent& evt) 
{
	switch (evt.GetId()) {
	case wxID_FX_ATTR:
		fxEditor->setFxParameter(ATTRIBUTE, evt.GetInt());
		break;
	case wxID_FX_FORM:
		fxEditor->setFxParameter(WAVEFORM, evt.GetInt());
		break;
	case wxID_FX_DIR:
		fxEditor->setFxParameter(DIRECTION, evt.GetInt());
		break;
	}

	//setUpdatedValues();
	evt.Skip();
}


void EffectWindow::setUpdatedValues() {
	attrBox->SetSelection(static_cast<int>(curFx->attribute));
	formBox->SetSelection(static_cast<int>(curFx->form));
	speedVal->SetValue(wxString::Format(wxT("%.2f"), curFx->bpm));
	rateVal->SetValue(wxString::Format(wxT("%.2f"), curFx->rate));
	dirBox->SetSelection(static_cast<int>(curFx->direction));
	groupsVal->SetValue(wxString::Format(wxT("%i"), curFx->groups));
	highVal->SetValue(wxString::Format(wxT("%i"), curFx->high));
	lowVal->SetValue(wxString::Format(wxT("%i"), curFx->low));
	dutyVal->SetValue(wxString::Format(wxT("%i"), curFx->dutycycle));
	wingsVal->SetValue(wxString::Format(wxT("%i"), curFx->wings));
	dimVal->SetValue(wxString::Format(wxT("%i"), curFx->color.d));
	redVal->SetValue(wxString::Format(wxT("%i"), curFx->color.r));
	greenVal->SetValue(wxString::Format(wxT("%i"), curFx->color.g));
	blueVal->SetValue(wxString::Format(wxT("%i"), curFx->color.b));
}
