#include "MainWindow.h"

#include "../Core/Core.hpp"

wxBEGIN_EVENT_TABLE(MainWindow, wxFrame)
EVT_MENU(101, MainWindow::onMenuClick)
EVT_MENU(102, MainWindow::onMenuClick)
EVT_BUTTON(wxID_MODE_STATCOL, MainWindow::OnButtonClicked)
EVT_BUTTON(wxID_MODE_RAINBOW, MainWindow::OnButtonClicked)
EVT_BUTTON(wxID_MODE_EFFECT, MainWindow::OnButtonClicked)
EVT_LISTBOX(wxID_EFFECT_LIST, MainWindow::onListBoxSelect)
EVT_SLIDER(wxID_SLD_STATCOL_DIM, MainWindow::sliderChange)
EVT_SLIDER(wxID_SLD_STATCOL_RED, MainWindow::sliderChange)
EVT_SLIDER(wxID_SLD_STATCOL_GREEN, MainWindow::sliderChange)
EVT_SLIDER(wxID_SLD_STATCOL_BLUE, MainWindow::sliderChange)
EVT_TOOL(wxID_TOOL_ADD, MainWindow::onToolClick)
EVT_TOOL(wxID_TOOL_EDIT, MainWindow::onToolClick)
EVT_TOOL(wxID_TOOL_DELETE, MainWindow::onToolClick)
EVT_TOOL(wxID_TOOL_NEXT, MainWindow::onToolClick)
EVT_TOOL(wxID_TOOL_PREVIOUS, MainWindow::onToolClick)
wxEND_EVENT_TABLE()



MainWindow::MainWindow()
    : wxFrame(nullptr, wxID_ANY, "ShowRoom Main Window",
              wxPoint(20, 150), wxSize(510, 560))
{
	menubar = new wxMenuBar;
	fileMenu = new wxMenu;
	toolMenu = new wxMenu;

	fileMenu->Append(wxID_EXIT, wxT("&Quit"));
	toolMenu->Append(101, wxT("Ledstrip Configuration"));
	toolMenu->Append(102, wxT("&Effect Engine"));
	menubar->Append(fileMenu, wxT("&File"));
	menubar->Append(toolMenu, wxT("&Tools"));
	SetMenuBar(menubar);

	// initialize toolbar using local function
	initToolbar();

	//wxPanel* panel1 = new wxPanel(this);
	//wxStaticBoxSizer* size1 = new wxStaticBoxSizer(wxVERTICAL, panel1, "Box");
	//size1->Add(new wxStaticText(size1->GetStaticBox(), wxID_ANY,
	//	"This window is a child of the staticbox"));

	//size1->FitInside(this);

	m_btn1 = new wxButton(this, wxID_MODE_STATCOL, "Static Color", wxPoint(10, 10), wxSize(150, 40));
	m_btn2 = new wxButton(this, wxID_MODE_RAINBOW, "Rainbow", wxPoint(170, 10), wxSize(150, 40));
	m_btn3 = new wxButton(this, wxID_MODE_EFFECT, "Effect", wxPoint(330, 10), wxSize(150, 40));

	//size1->Add(m_btn1);
	//size1->Add(m_btn2);
	//size1->Add(m_btn3);

	fxList = new wxListBox(this, wxID_EFFECT_LIST, wxPoint(10, 90), wxSize(200, 170));
	//Temporary add 2 effects for testing
	effectList = SR::getMainEngine()->getEffectList();
	for (auto it : *effectList) {
		fxList->AppendString(it.name);
	}
	fxList->SetSelection(0);

	sld1 = new wxSlider(this, wxID_SLD_STATCOL_DIM, 255, 0, 255, wxPoint(10, 270), wxSize(475, 15), wxSL_VALUE_LABEL);
	sld2 = new wxSlider(this, wxID_SLD_STATCOL_RED, 255, 0, 255, wxPoint(10, 320), wxSize(475, 15), wxSL_VALUE_LABEL);
	sld3 = new wxSlider(this, wxID_SLD_STATCOL_GREEN, 255, 0, 255, wxPoint(10, 370), wxSize(475, 15), wxSL_VALUE_LABEL);
	sld4 = new wxSlider(this, wxID_SLD_STATCOL_BLUE, 255, 0, 255, wxPoint(10, 420), wxSize(475, 15), wxSL_VALUE_LABEL);
}


MainWindow::~MainWindow() 
{
}


void MainWindow::initToolbar() {
	// Create the new toolbar
	wxToolBar* toolbar = CreateToolBar();

	// Populate the toolbar
	// bitmap array
	wxBitmap toolBarBitmaps[3];

	// Function to import from files (included in header)
#define INIT_TOOL_BMP(bmp) \
        toolBarBitmaps[Tool_##bmp] = wxBitmap(bmp##_xpm)

	INIT_TOOL_BMP(add);
	INIT_TOOL_BMP(edit);
	INIT_TOOL_BMP(delete);
	//INIT_TOOL_BMP(next);
	//INIT_TOOL_BMP(previous);

	//add tools

	toolbar->AddTool(wxID_TOOL_ADD, wxT("add effect"), toolBarBitmaps[Tool_add]);
	toolbar->AddTool(wxID_TOOL_EDIT, wxT("edit effect"), toolBarBitmaps[Tool_edit]);
	toolbar->AddTool(wxID_TOOL_DELETE, wxT("delete effect"), toolBarBitmaps[Tool_delete]);
	//toolbar->AddTool(wxID_TOOL_NEXT, wxT("next"), toolBarBitmaps[Tool_next]);
	//toolbar->AddTool(wxID_TOOL_PREVIOUS, wxT("previous"), toolBarBitmaps[Tool_previous]);

	// Realize toolbar after all tools are added
	toolbar->Realize();
}



// *************************************************************
//						Event handling
// *************************************************************

// Menu events
void MainWindow::onMenuClick(wxCommandEvent& evt)
{
	wxLogDebug("<MainWindow> Menu clicked, id: %d", evt.GetId());
	switch (evt.GetId())
	{
	case 101:
		configWindow = new ConfigWindow(SR::getMainEngine(), SR::getDataGate());
		configWindow->Show();
		break;
	case 102:	//open 
		effectWindow = new EffectWindow(SR::getMainEngine());
		effectWindow->Show();
		break;
	}
	evt.Skip();
}


// Toolbar events
void MainWindow::onToolClick(wxCommandEvent& evt)
{
	wxLogDebug("<MainWindow> toolbutton pressed: %i", evt.GetId());
    SR::getMainEngine()->toolBarInput(evt.GetId());

	switch (evt.GetId())
	{
	case wxID_TOOL_ADD:
		// 
		break;	
	case wxID_TOOL_EDIT:
		effectWindow = new EffectWindow(SR::getMainEngine());
		effectWindow->Show();
		break;	
	case wxID_TOOL_DELETE:
		// 
		break;
	}

	evt.Skip();
}


// Buttons
void MainWindow::OnButtonClicked(wxCommandEvent& evt)
{
	int buttonId = evt.GetId();
	//wxLogDebug("<main frame> Button %d pressed", buttonId);

    SR::getMainEngine()->inputBtnChange(buttonId);
	evt.Skip();
}


// Sliders
void MainWindow::sliderChange(wxCommandEvent& evt) 
{
	//wxLogDebug("<main frame> Slider changed, value: %d", evt.GetInt());
    SR::getMainEngine()->inputSldChange(evt.GetId(), evt.GetInt());
	evt.Skip();
}


// Listbox
void MainWindow::onListBoxSelect(wxCommandEvent& evt) 
{
	wxLogDebug("<MainWindow> Listbox item evt, number: %d", evt.GetInt());
    SR::getMainEngine()->effectSelection(evt.GetInt());
}


// On quit
void MainWindow::OnQuit(wxCommandEvent& WXUNUSED(evt))
{
	Close(true);
}
