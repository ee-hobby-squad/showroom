#include "ConfigWindow.h"

#include <functional>

#include "../Engine/Config/Configuration.h"

wxBEGIN_EVENT_TABLE(ConfigWindow, wxFrame)
EVT_BUTTON(wxID_APPLY, ConfigWindow::onApplyClicked)
wxEND_EVENT_TABLE()


ConfigWindow::ConfigWindow(MainEngine* engine, DataGate *gate)
    : wxFrame(nullptr,
              wxID_ANY,
              "Configuration",
              wxPoint(500, 150),
              wxSize(510, 400)),
      mMainEngine(engine), mDataGate(gate)
{
    loadConfig();
    createWidgets();
}


ConfigWindow::~ConfigWindow() 
{

}


void ConfigWindow::createWidgets()
{
    wxBoxSizer *wrapperSizer = new wxBoxSizer(wxVERTICAL); // For borders

    wxFlexGridSizer *mainSizer = new wxFlexGridSizer(2, 5, 10);
    mainSizer->AddGrowableCol(0, 1);
    mainSizer->AddGrowableCol(1);
    
    wxString communicationProviders[] = {
        wxT("Serial"),
        wxT("UDP")
    };
    mCommunicationProviderTooltip.Create(this, wxWindow::NewControlId(),
                                "Communication Type", wxDefaultPosition,
                                wxSize(-1, -1));
    mCommunicationProviderDropDown.Create(this, wxWindow::NewControlId(),
                                wxDefaultPosition, wxSize(180, -1), 2,
                                communicationProviders);
    mCommunicationProviderDropDown.SetSelection(
            mCommunicationProviderDropDown.FindString(mConfig.communicationProvider));

    mUDPServerIPTooltip.Create(this, wxWindow::NewControlId(), "Server IP",
                               wxDefaultPosition, wxSize(-1, -1));
    mUDPServerIPInput.Create(this, wxWindow::NewControlId(), mConfig.udpServerIp,
                             wxDefaultPosition, wxSize(150, -1));
    mUDPServerPortTooltip.Create(this, wxWindow::NewControlId(), ":",
                                 wxDefaultPosition, wxSize(-1, -1));
    mUDPServerPortInput.Create(this, wxWindow::NewControlId(), "",
                               wxDefaultPosition, wxSize(90, -1), wxSP_ARROW_KEYS,
                               1, 65535, mConfig.udpServerPort);

    wxBoxSizer *serverIpSizer = new wxBoxSizer(wxHORIZONTAL);
    serverIpSizer->Add(&mUDPServerIPInput);
    serverIpSizer->Add(&mUDPServerPortTooltip, 0, wxALIGN_CENTER_VERTICAL);
    serverIpSizer->Add(&mUDPServerPortInput);

    mUDPClientIPTooltip.Create(this, wxWindow::NewControlId(), "ESP IP",
                               wxDefaultPosition, wxSize(-1, -1));
    mUDPClientIPInput.Create(this, wxWindow::NewControlId(), mConfig.udpClientIp,
                             wxDefaultPosition, wxSize(150, -1));
    mUDPClientPortTooltip.Create(this, wxWindow::NewControlId(), ":",
                                 wxDefaultPosition, wxSize(-1, -1));
    mUDPClientPortInput.Create(this, wxWindow::NewControlId(), "",
                               wxDefaultPosition, wxSize(90, -1), wxSP_ARROW_KEYS,
                               1, 65535, mConfig.udpClientPort);

    wxBoxSizer *clientIpSizer = new wxBoxSizer(wxHORIZONTAL);
    clientIpSizer->Add(&mUDPClientIPInput);
    clientIpSizer->Add(&mUDPClientPortTooltip, 0, wxALIGN_CENTER_VERTICAL);
    clientIpSizer->Add(&mUDPClientPortInput);

    mSerialPortTooltip.Create(this, wxWindow::NewControlId(), "Serial Port",
                              wxDefaultPosition, wxSize(-1, -1));
    mSerialPortInput.Create(this, wxWindow::NewControlId(), mConfig.serialPort,
                            wxDefaultPosition, wxSize(130, -1));
    mSerialBaudRateTooltip.Create(this, wxWindow::NewControlId(), "Baud",
                                  wxDefaultPosition, wxSize(-1, -1));
    mSerialBaudRateInput.Create(this, wxWindow::NewControlId(), "",
                                wxDefaultPosition, wxSize(90, -1), wxSP_ARROW_KEYS,
                                0, 1e9, mConfig.serialBaudRate);

    mNumberOfLedsTooltip.Create(this, wxWindow::NewControlId(),
                                "Number of LEDS", wxDefaultPosition,
                                wxSize(-1, -1));
    mNumberOfLedsInput.Create(this, wxWindow::NewControlId(), "",
                              wxDefaultPosition, wxSize(80, -1), wxSP_ARROW_KEYS,
                              1, 600, mConfig.numberOfLeds);
    mSaveButton.Create(this, wxID_APPLY, wxEmptyString, wxDefaultPosition,
                       wxSize(80, -1));

    mainSizer->Add(&mCommunicationProviderTooltip, 0, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
    mainSizer->Add(&mCommunicationProviderDropDown, 0, wxEXPAND);

    mainSizer->Add(&mUDPServerIPTooltip, 0, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
    mainSizer->Add(serverIpSizer, 0, wxEXPAND);

    mainSizer->Add(&mUDPClientIPTooltip, 0, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
    mainSizer->Add(clientIpSizer, 0, wxEXPAND);

    mainSizer->Add(&mSerialPortTooltip, 0, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
    mainSizer->Add(&mSerialPortInput, 0, wxEXPAND);
    mainSizer->Add(&mSerialBaudRateTooltip, 0, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
    mainSizer->Add(&mSerialBaudRateInput, 0, wxEXPAND);
    mainSizer->Add(&mNumberOfLedsTooltip, 0, wxALIGN_CENTER_VERTICAL | wxALIGN_RIGHT);
    mainSizer->Add(&mNumberOfLedsInput, 0, wxEXPAND);
    mainSizer->Add(0,0);
    mainSizer->Add(&mSaveButton, 0, wxALIGN_RIGHT | wxTOP, 50);

    wrapperSizer->Add(mainSizer, 0, wxEXPAND | wxALL, 10);
    SetSizerAndFit(wrapperSizer);
}

void ConfigWindow::loadConfig()
{
    mConfig = ConfigurationManager::loadConfig();
}

void ConfigWindow::onApplyClicked(wxCommandEvent& evt)
{
    (void)evt; // This is unused casting it to a void removes the warning
    mConfig.numberOfLeds = mNumberOfLedsInput.GetValue();

    int selected = mCommunicationProviderDropDown.GetSelection();
    auto val = mCommunicationProviderDropDown.GetString(selected);
    if (val == "UDP")
    {
        mConfig.communicationProvider = "UDP";
    }
    else if (val == "Serial")
    {
        mConfig.communicationProvider = "Serial";
    }

    mConfig.udpServerIp = mUDPServerIPInput.GetValue();
    mConfig.udpServerPort = mUDPServerPortInput.GetValue();

    mConfig.udpClientIp = mUDPClientIPInput.GetValue();
    mConfig.udpClientPort = mUDPClientPortInput.GetValue();

    mConfig.serialPort = mSerialPortInput.GetValue();
    mConfig.serialBaudRate = mSerialBaudRateInput.GetValue();

    ConfigurationManager::saveToFile("userdata/showroom.cfg", mConfig);

    if (val == "UDP")
    {
        mDataGate->setUDPCommunicationProvider();
    }
    else if (val == "Serial")
    {
        mDataGate->setSerialCommunicationProvider();
    }

    wxLogDebug("<ConfigWindow> Settings applied!");

    mMainEngine->onSettingsChange();
}
