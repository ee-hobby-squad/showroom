#pragma once

#include "wx/wx.h"
#include "wxIDs.h"
#include "../Engine/MainEngine.h"

class EffectWindow : public wxFrame
{
public:
	EffectWindow(MainEngine *mainEngine);

	wxChoice* attrBox = nullptr;
	wxChoice* formBox = nullptr;
	wxChoice* dirBox = nullptr;
	wxStaticText* attrTxt = nullptr;
	wxStaticText* formTxt = nullptr;
	wxStaticText* dirTxt = nullptr;

	wxTextCtrl* dimVal = nullptr;
	wxTextCtrl* redVal = nullptr;
	wxTextCtrl* greenVal = nullptr;
	wxTextCtrl* blueVal = nullptr;
	wxStaticText* dimTxt = nullptr;
	wxStaticText* colorTxt = nullptr;

	wxTextCtrl* speedVal = nullptr;
	wxTextCtrl* rateVal = nullptr;
	wxTextCtrl* groupsVal = nullptr;
	wxStaticText* speedTxt = nullptr;
	wxStaticText* rateTxt = nullptr;
	wxStaticText* groupsTxt = nullptr;

	wxStaticText* highTxt = nullptr;
	wxStaticText* lowTxt = nullptr;
	wxStaticText* dutyTxt = nullptr;
	wxTextCtrl* highVal = nullptr;
	wxTextCtrl* lowVal = nullptr;
	wxTextCtrl* dutyVal = nullptr;

	wxStaticText* wingsTxt = nullptr;
	wxTextCtrl* wingsVal = nullptr;


	void OnCloseWindow(wxCloseEvent& evt);
	void TextValueChanged(wxCommandEvent& evt);
	void ChoiceValueChanged(wxCommandEvent& evt);

	wxDECLARE_EVENT_TABLE();
	

private:
	void setUpdatedValues();
	FxEditor* fxEditor = nullptr;
    MainEngine *mainEngine = nullptr;
	Effect* curFx;
	bool windowInit = false;
};


