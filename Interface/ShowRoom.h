#pragma once

#include "wx/wx.h"
#include <optional>

#include "MainWindow.h"

class ShowRoom : public wxApp
{
public:
	virtual bool OnInit();
    virtual void OnInitCmdLine(wxCmdLineParser &parser);
    virtual bool OnCmdLineParsed(wxCmdLineParser &parser);
	~ShowRoom() = default;
private:
	MainWindow* mMainFrame = nullptr;
};
