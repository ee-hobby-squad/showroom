#include "ShowRoom.h"

#include "wx/cmdline.h"

#ifndef NO_MAIN
bool ShowRoom::OnInit() {
	wxLog* logger = new wxLogStream();
	wxLog::SetActiveTarget(logger);

    if (!wxApp::OnInit()) return false;

	mMainFrame = new MainWindow();
	mMainFrame->Show();

	return true;
}

extern const wxCmdLineEntryDesc parserDesc[];

void ShowRoom::OnInitCmdLine(wxCmdLineParser &parser)
{
    parser.SetDesc(parserDesc);
    parser.SetSwitchChars("-");
}

bool ShowRoom::OnCmdLineParsed([[maybe_unused]]wxCmdLineParser &parser)
{
    return true;
}
#endif
